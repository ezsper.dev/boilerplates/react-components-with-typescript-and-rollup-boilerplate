exports["MyLibrary"] =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/helpers/defineProperty");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/helpers/objectWithoutProperties");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("classnames");

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin
module.exports = {"button":"vMMeQJjR7cnoBSrl71AtM"};

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: external "@babel/runtime/helpers/defineProperty"
var defineProperty_ = __webpack_require__(1);
var defineProperty_default = /*#__PURE__*/__webpack_require__.n(defineProperty_);

// EXTERNAL MODULE: external "@babel/runtime/helpers/objectWithoutProperties"
var objectWithoutProperties_ = __webpack_require__(2);
var objectWithoutProperties_default = /*#__PURE__*/__webpack_require__.n(objectWithoutProperties_);

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(0);
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: external "classnames"
var external_classnames_ = __webpack_require__(3);
var external_classnames_default = /*#__PURE__*/__webpack_require__.n(external_classnames_);

// EXTERNAL MODULE: ./components/Button/styles.css
var styles = __webpack_require__(4);
var styles_default = /*#__PURE__*/__webpack_require__.n(styles);

// CONCATENATED MODULE: ./components/Button/index.tsx



function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { defineProperty_default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }




const CustomButton = component => Object(external_react_["forwardRef"])((_ref, ref) => {
  let {
    className
  } = _ref,
      rest = objectWithoutProperties_default()(_ref, ["className"]);

  return external_react_default.a.createElement(component, _objectSpread({
    className: external_classnames_default()(styles_default.a.button, className)
  }, rest, {
    ref
  }));
});
const BaseButton = CustomButton('button');
const ButtonLink = CustomButton('a');
const Button = Object.assign(BaseButton, {
  Link: ButtonLink
});
/* harmony default export */ var components_Button = (Button);

try {
  // @ts-ignore
  CustomButton.displayName = "CustomButton"; // @ts-ignore

  CustomButton.__docgenInfo = {
    "description": "",
    "displayName": "CustomButton",
    "props": {
      "toString": {
        "defaultValue": null,
        "description": "Returns a string representation of a string.",
        "name": "toString",
        "required": true,
        "type": {
          "name": "() => string"
        }
      },
      "charAt": {
        "defaultValue": null,
        "description": "Returns the character at the specified index.\n@param pos The zero-based index of the desired character.",
        "name": "charAt",
        "required": true,
        "type": {
          "name": "(pos: number) => string"
        }
      },
      "charCodeAt": {
        "defaultValue": null,
        "description": "Returns the Unicode value of the character at the specified location.\n@param index The zero-based index of the desired character. If there is no character at the specified index, NaN is returned.",
        "name": "charCodeAt",
        "required": true,
        "type": {
          "name": "(index: number) => number"
        }
      },
      "concat": {
        "defaultValue": null,
        "description": "Returns a string that contains the concatenation of two or more strings.\n@param strings The strings to append to the end of the string.",
        "name": "concat",
        "required": true,
        "type": {
          "name": "(...strings: string[]) => string"
        }
      },
      "indexOf": {
        "defaultValue": null,
        "description": "Returns the position of the first occurrence of a substring.\n@param searchString The substring to search for in the string\n@param position The index at which to begin searching the String object. If omitted, search starts at the beginning of the string.",
        "name": "indexOf",
        "required": true,
        "type": {
          "name": "(searchString: string, position?: number) => number"
        }
      },
      "lastIndexOf": {
        "defaultValue": null,
        "description": "Returns the last occurrence of a substring in the string.\n@param searchString The substring to search for.\n@param position The index at which to begin searching. If omitted, the search begins at the end of the string.",
        "name": "lastIndexOf",
        "required": true,
        "type": {
          "name": "(searchString: string, position?: number) => number"
        }
      },
      "localeCompare": {
        "defaultValue": null,
        "description": "Determines whether two strings are equivalent in the current locale.\nDetermines whether two strings are equivalent in the current or specified locale.\n@param that String to compare to target string\n@param that String to compare to target string\n@param locales A locale string or array of locale strings that contain one or more language or locale tags. If you include more than one locale string, list them in descending order of priority so that the first entry is the preferred locale. If you omit this parameter, the default locale of the JavaScript runtime is used. This parameter must conform to BCP 47 standards; see the Intl.Collator object for details.\n@param options An object that contains one or more properties that specify comparison options. see the Intl.Collator object for details.",
        "name": "localeCompare",
        "required": true,
        "type": {
          "name": "{ (that: string): number; (that: string, locales?: string | string[], options?: CollatorOptions): number; }"
        }
      },
      "match": {
        "defaultValue": null,
        "description": "Matches a string with a regular expression, and returns an array containing the results of that search.\nMatches a string an object that supports being matched against, and returns an array containing the results of that search.\n@param regexp A variable name or string literal containing the regular expression pattern and flags.\n@param matcher An object that supports being matched against.",
        "name": "match",
        "required": true,
        "type": {
          "name": "{ (regexp: string | RegExp): RegExpMatchArray; (matcher: { [Symbol.match](string: string): RegExpMatchArray; }): RegExpMatchArray; }"
        }
      },
      "replace": {
        "defaultValue": null,
        "description": "Replaces text in a string, using a regular expression or search string.\nReplaces text in a string, using a regular expression or search string.\nReplaces text in a string, using an object that supports replacement within a string.\nReplaces text in a string, using an object that supports replacement within a string.\n@param searchValue A string to search for.\n@param replaceValue A string containing the text to replace for every successful match of searchValue in this string.\n@param searchValue A string to search for.\n@param replacer A function that returns the replacement text.\n@param searchValue A object can search for and replace matches within a string.\n@param replaceValue A string containing the text to replace for every successful match of searchValue in this string.\n@param searchValue A object can search for and replace matches within a string.\n@param replacer A function that returns the replacement text.",
        "name": "replace",
        "required": true,
        "type": {
          "name": "{ (searchValue: string | RegExp, replaceValue: string): string; (searchValue: string | RegExp, replacer: (substring: string, ...args: any[]) => string): string; (searchValue: { [Symbol.replace](string: string, replaceValue: string): string; }, replaceValue: string): string; (searchValue: { ...; }, replacer: (substri..."
        }
      },
      "search": {
        "defaultValue": null,
        "description": "Finds the first substring match in a regular expression search.\nFinds the first substring match in a regular expression search.\n@param regexp The regular expression pattern and applicable flags.\n@param searcher An object which supports searching within a string.",
        "name": "search",
        "required": true,
        "type": {
          "name": "{ (regexp: string | RegExp): number; (searcher: { [Symbol.search](string: string): number; }): number; }"
        }
      },
      "slice": {
        "defaultValue": null,
        "description": "Returns a section of a string.\n@param start The index to the beginning of the specified portion of stringObj.\n@param end The index to the end of the specified portion of stringObj. The substring includes the characters up to, but not including, the character indicated by end.\rIf this value is not specified, the substring continues to the end of stringObj.",
        "name": "slice",
        "required": true,
        "type": {
          "name": "(start?: number, end?: number) => string"
        }
      },
      "split": {
        "defaultValue": null,
        "description": "Split a string into substrings using the specified separator and return them as an array.\nSplit a string into substrings using the specified separator and return them as an array.\n@param separator A string that identifies character or characters to use in separating the string. If omitted, a single-element array containing the entire string is returned.\n@param limit A value used to limit the number of elements returned in the array.\n@param splitter An object that can split a string.\n@param limit A value used to limit the number of elements returned in the array.",
        "name": "split",
        "required": true,
        "type": {
          "name": "{ (separator: string | RegExp, limit?: number): string[]; (splitter: { [Symbol.split](string: string, limit?: number): string[]; }, limit?: number): string[]; }"
        }
      },
      "substring": {
        "defaultValue": null,
        "description": "Returns the substring at the specified location within a String object.\n@param start The zero-based index number indicating the beginning of the substring.\n@param end Zero-based index number indicating the end of the substring. The substring includes the characters up to, but not including, the character indicated by end.\rIf end is omitted, the characters from start through the end of the original string are returned.",
        "name": "substring",
        "required": true,
        "type": {
          "name": "(start: number, end?: number) => string"
        }
      },
      "toLowerCase": {
        "defaultValue": null,
        "description": "Converts all the alphabetic characters in a string to lowercase.",
        "name": "toLowerCase",
        "required": true,
        "type": {
          "name": "() => string"
        }
      },
      "toLocaleLowerCase": {
        "defaultValue": null,
        "description": "Converts all alphabetic characters to lowercase, taking into account the host environment's current locale.",
        "name": "toLocaleLowerCase",
        "required": true,
        "type": {
          "name": "() => string"
        }
      },
      "toUpperCase": {
        "defaultValue": null,
        "description": "Converts all the alphabetic characters in a string to uppercase.",
        "name": "toUpperCase",
        "required": true,
        "type": {
          "name": "() => string"
        }
      },
      "toLocaleUpperCase": {
        "defaultValue": null,
        "description": "Returns a string where all alphabetic characters have been converted to uppercase, taking into account the host environment's current locale.",
        "name": "toLocaleUpperCase",
        "required": true,
        "type": {
          "name": "() => string"
        }
      },
      "trim": {
        "defaultValue": null,
        "description": "Removes the leading and trailing white space and line terminator characters from a string.",
        "name": "trim",
        "required": true,
        "type": {
          "name": "() => string"
        }
      },
      "length": {
        "defaultValue": null,
        "description": "Returns the length of a String object.",
        "name": "length",
        "required": true,
        "type": {
          "name": "number"
        }
      },
      "substr": {
        "defaultValue": null,
        "description": "Gets a substring beginning at the specified location and having the specified length.\n@param from The starting position of the desired substring. The index of the first character in the string is zero.\n@param length The number of characters to include in the returned substring.",
        "name": "substr",
        "required": true,
        "type": {
          "name": "(from: number, length?: number) => string"
        }
      },
      "valueOf": {
        "defaultValue": null,
        "description": "Returns the primitive value of the specified object.",
        "name": "valueOf",
        "required": true,
        "type": {
          "name": "() => string"
        }
      },
      "codePointAt": {
        "defaultValue": null,
        "description": "Returns a nonnegative integer Number less than 1114112 (0x110000) that is the code point\nvalue of the UTF-16 encoded code point starting at the string element at position pos in\r\nthe String resulting from converting this object to a String.\r\nIf there is no element at that position, the result is undefined.\r\nIf a valid UTF-16 surrogate pair does not begin at pos, the result is the code unit at pos.",
        "name": "codePointAt",
        "required": true,
        "type": {
          "name": "(pos: number) => number"
        }
      },
      "includes": {
        "defaultValue": null,
        "description": "Returns true if searchString appears as a substring of the result of converting this\nobject to a String, at one or more positions that are\r\ngreater than or equal to position; otherwise, returns false.\n@param searchString search string\n@param position If position is undefined, 0 is assumed, so as to search all of the String.",
        "name": "includes",
        "required": true,
        "type": {
          "name": "(searchString: string, position?: number) => boolean"
        }
      },
      "endsWith": {
        "defaultValue": null,
        "description": "Returns true if the sequence of elements of searchString converted to a String is the\nsame as the corresponding elements of this object (converted to a String) starting at\r\nendPosition \u2013 length(this). Otherwise returns false.",
        "name": "endsWith",
        "required": true,
        "type": {
          "name": "(searchString: string, endPosition?: number) => boolean"
        }
      },
      "normalize": {
        "defaultValue": null,
        "description": "Returns the String value result of normalizing the string into the normalization form\nnamed by form as specified in Unicode Standard Annex #15, Unicode Normalization Forms.\nReturns the String value result of normalizing the string into the normalization form\r\nnamed by form as specified in Unicode Standard Annex #15, Unicode Normalization Forms.\n@param form Applicable values: \"NFC\", \"NFD\", \"NFKC\", or \"NFKD\", If not specified default\ris \"NFC\"\n@param form Applicable values: \"NFC\", \"NFD\", \"NFKC\", or \"NFKD\", If not specified default\ris \"NFC\"",
        "name": "normalize",
        "required": true,
        "type": {
          "name": "{ (form: \"NFC\" | \"NFD\" | \"NFKC\" | \"NFKD\"): string; (form?: string): string; }"
        }
      },
      "repeat": {
        "defaultValue": null,
        "description": "Returns a String value that is made from count copies appended together. If count is 0,\nthe empty string is returned.\n@param count number of copies to append",
        "name": "repeat",
        "required": true,
        "type": {
          "name": "(count: number) => string"
        }
      },
      "startsWith": {
        "defaultValue": null,
        "description": "Returns true if the sequence of elements of searchString converted to a String is the\nsame as the corresponding elements of this object (converted to a String) starting at\r\nposition. Otherwise returns false.",
        "name": "startsWith",
        "required": true,
        "type": {
          "name": "(searchString: string, position?: number) => boolean"
        }
      },
      "anchor": {
        "defaultValue": null,
        "description": "Returns an <a> HTML anchor element and sets the name attribute to the text value\n@param name",
        "name": "anchor",
        "required": true,
        "type": {
          "name": "(name: string) => string"
        }
      },
      "big": {
        "defaultValue": null,
        "description": "Returns a <big> HTML element",
        "name": "big",
        "required": true,
        "type": {
          "name": "() => string"
        }
      },
      "blink": {
        "defaultValue": null,
        "description": "Returns a <blink> HTML element",
        "name": "blink",
        "required": true,
        "type": {
          "name": "() => string"
        }
      },
      "bold": {
        "defaultValue": null,
        "description": "Returns a <b> HTML element",
        "name": "bold",
        "required": true,
        "type": {
          "name": "() => string"
        }
      },
      "fixed": {
        "defaultValue": null,
        "description": "Returns a <tt> HTML element",
        "name": "fixed",
        "required": true,
        "type": {
          "name": "() => string"
        }
      },
      "fontcolor": {
        "defaultValue": null,
        "description": "Returns a <font> HTML element and sets the color attribute value",
        "name": "fontcolor",
        "required": true,
        "type": {
          "name": "(color: string) => string"
        }
      },
      "fontsize": {
        "defaultValue": null,
        "description": "Returns a <font> HTML element and sets the size attribute value\nReturns a <font> HTML element and sets the size attribute value",
        "name": "fontsize",
        "required": true,
        "type": {
          "name": "{ (size: number): string; (size: string): string; }"
        }
      },
      "italics": {
        "defaultValue": null,
        "description": "Returns an <i> HTML element",
        "name": "italics",
        "required": true,
        "type": {
          "name": "() => string"
        }
      },
      "link": {
        "defaultValue": null,
        "description": "Returns an <a> HTML element and sets the href attribute value",
        "name": "link",
        "required": true,
        "type": {
          "name": "(url: string) => string"
        }
      },
      "small": {
        "defaultValue": null,
        "description": "Returns a <small> HTML element",
        "name": "small",
        "required": true,
        "type": {
          "name": "() => string"
        }
      },
      "strike": {
        "defaultValue": null,
        "description": "Returns a <strike> HTML element",
        "name": "strike",
        "required": true,
        "type": {
          "name": "() => string"
        }
      },
      "sub": {
        "defaultValue": null,
        "description": "Returns a <sub> HTML element",
        "name": "sub",
        "required": true,
        "type": {
          "name": "() => string"
        }
      },
      "sup": {
        "defaultValue": null,
        "description": "Returns a <sup> HTML element",
        "name": "sup",
        "required": true,
        "type": {
          "name": "() => string"
        }
      },
      "__@iterator": {
        "defaultValue": null,
        "description": "Iterator",
        "name": "__@iterator",
        "required": true,
        "type": {
          "name": "() => IterableIterator<string>"
        }
      },
      "padStart": {
        "defaultValue": null,
        "description": "Pads the current string with a given string (possibly repeated) so that the resulting string reaches a given length.\nThe padding is applied from the start (left) of the current string.\n@param maxLength The length of the resulting string once the current string has been padded.\rIf this parameter is smaller than the current string's length, the current string will be returned as it is.\n@param fillString The string to pad the current string with.\rIf this string is too long, it will be truncated and the left-most part will be applied.\rThe default value for this parameter is \" \" (U+0020).",
        "name": "padStart",
        "required": true,
        "type": {
          "name": "(maxLength: number, fillString?: string) => string"
        }
      },
      "padEnd": {
        "defaultValue": null,
        "description": "Pads the current string with a given string (possibly repeated) so that the resulting string reaches a given length.\nThe padding is applied from the end (right) of the current string.\n@param maxLength The length of the resulting string once the current string has been padded.\rIf this parameter is smaller than the current string's length, the current string will be returned as it is.\n@param fillString The string to pad the current string with.\rIf this string is too long, it will be truncated and the left-most part will be applied.\rThe default value for this parameter is \" \" (U+0020).",
        "name": "padEnd",
        "required": true,
        "type": {
          "name": "(maxLength: number, fillString?: string) => string"
        }
      },
      "trimLeft": {
        "defaultValue": null,
        "description": "Removes whitespace from the left end of a string.",
        "name": "trimLeft",
        "required": true,
        "type": {
          "name": "() => string"
        }
      },
      "trimRight": {
        "defaultValue": null,
        "description": "Removes whitespace from the right end of a string.",
        "name": "trimRight",
        "required": true,
        "type": {
          "name": "() => string"
        }
      }
    }
  }; // @ts-ignore

  if (typeof STORYBOOK_REACT_CLASSES !== "undefined") // @ts-ignore
    STORYBOOK_REACT_CLASSES["components/Button/index.tsx#CustomButton"] = {
      docgenInfo: CustomButton.__docgenInfo,
      name: "CustomButton",
      path: "components/Button/index.tsx#CustomButton"
    };
} catch (__react_docgen_typescript_loader_error) {}

try {
  // @ts-ignore
  ButtonLink.displayName = "ButtonLink"; // @ts-ignore

  ButtonLink.__docgenInfo = {
    "description": "",
    "displayName": "ButtonLink",
    "props": {
      "cite": {
        "defaultValue": null,
        "description": "",
        "name": "cite",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "data": {
        "defaultValue": null,
        "description": "",
        "name": "data",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "form": {
        "defaultValue": null,
        "description": "",
        "name": "form",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "label": {
        "defaultValue": null,
        "description": "",
        "name": "label",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "span": {
        "defaultValue": null,
        "description": "",
        "name": "span",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "style": {
        "defaultValue": null,
        "description": "",
        "name": "style",
        "required": false,
        "type": {
          "name": "CSSProperties"
        }
      },
      "summary": {
        "defaultValue": null,
        "description": "",
        "name": "summary",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "title": {
        "defaultValue": null,
        "description": "",
        "name": "title",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "className": {
        "defaultValue": null,
        "description": "",
        "name": "className",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "accept": {
        "defaultValue": null,
        "description": "",
        "name": "accept",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "acceptCharset": {
        "defaultValue": null,
        "description": "",
        "name": "acceptCharset",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "action": {
        "defaultValue": null,
        "description": "",
        "name": "action",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "allowFullScreen": {
        "defaultValue": null,
        "description": "",
        "name": "allowFullScreen",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "allowTransparency": {
        "defaultValue": null,
        "description": "",
        "name": "allowTransparency",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "alt": {
        "defaultValue": null,
        "description": "",
        "name": "alt",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "as": {
        "defaultValue": null,
        "description": "",
        "name": "as",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "async": {
        "defaultValue": null,
        "description": "",
        "name": "async",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "autoComplete": {
        "defaultValue": null,
        "description": "",
        "name": "autoComplete",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "autoFocus": {
        "defaultValue": null,
        "description": "",
        "name": "autoFocus",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "autoPlay": {
        "defaultValue": null,
        "description": "",
        "name": "autoPlay",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "capture": {
        "defaultValue": null,
        "description": "",
        "name": "capture",
        "required": false,
        "type": {
          "name": "string | boolean"
        }
      },
      "cellPadding": {
        "defaultValue": null,
        "description": "",
        "name": "cellPadding",
        "required": false,
        "type": {
          "name": "ReactText"
        }
      },
      "cellSpacing": {
        "defaultValue": null,
        "description": "",
        "name": "cellSpacing",
        "required": false,
        "type": {
          "name": "ReactText"
        }
      },
      "charSet": {
        "defaultValue": null,
        "description": "",
        "name": "charSet",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "challenge": {
        "defaultValue": null,
        "description": "",
        "name": "challenge",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "checked": {
        "defaultValue": null,
        "description": "",
        "name": "checked",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "classID": {
        "defaultValue": null,
        "description": "",
        "name": "classID",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "cols": {
        "defaultValue": null,
        "description": "",
        "name": "cols",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "colSpan": {
        "defaultValue": null,
        "description": "",
        "name": "colSpan",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "content": {
        "defaultValue": null,
        "description": "",
        "name": "content",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "controls": {
        "defaultValue": null,
        "description": "",
        "name": "controls",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "coords": {
        "defaultValue": null,
        "description": "",
        "name": "coords",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "crossOrigin": {
        "defaultValue": null,
        "description": "",
        "name": "crossOrigin",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "dateTime": {
        "defaultValue": null,
        "description": "",
        "name": "dateTime",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "default": {
        "defaultValue": null,
        "description": "",
        "name": "default",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "defer": {
        "defaultValue": null,
        "description": "",
        "name": "defer",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "disabled": {
        "defaultValue": null,
        "description": "",
        "name": "disabled",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "download": {
        "defaultValue": null,
        "description": "",
        "name": "download",
        "required": false,
        "type": {
          "name": "any"
        }
      },
      "encType": {
        "defaultValue": null,
        "description": "",
        "name": "encType",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "formAction": {
        "defaultValue": null,
        "description": "",
        "name": "formAction",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "formEncType": {
        "defaultValue": null,
        "description": "",
        "name": "formEncType",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "formMethod": {
        "defaultValue": null,
        "description": "",
        "name": "formMethod",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "formNoValidate": {
        "defaultValue": null,
        "description": "",
        "name": "formNoValidate",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "formTarget": {
        "defaultValue": null,
        "description": "",
        "name": "formTarget",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "frameBorder": {
        "defaultValue": null,
        "description": "",
        "name": "frameBorder",
        "required": false,
        "type": {
          "name": "ReactText"
        }
      },
      "headers": {
        "defaultValue": null,
        "description": "",
        "name": "headers",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "height": {
        "defaultValue": null,
        "description": "",
        "name": "height",
        "required": false,
        "type": {
          "name": "ReactText"
        }
      },
      "high": {
        "defaultValue": null,
        "description": "",
        "name": "high",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "href": {
        "defaultValue": null,
        "description": "",
        "name": "href",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "hrefLang": {
        "defaultValue": null,
        "description": "",
        "name": "hrefLang",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "htmlFor": {
        "defaultValue": null,
        "description": "",
        "name": "htmlFor",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "httpEquiv": {
        "defaultValue": null,
        "description": "",
        "name": "httpEquiv",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "integrity": {
        "defaultValue": null,
        "description": "",
        "name": "integrity",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "keyParams": {
        "defaultValue": null,
        "description": "",
        "name": "keyParams",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "keyType": {
        "defaultValue": null,
        "description": "",
        "name": "keyType",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "kind": {
        "defaultValue": null,
        "description": "",
        "name": "kind",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "list": {
        "defaultValue": null,
        "description": "",
        "name": "list",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "loop": {
        "defaultValue": null,
        "description": "",
        "name": "loop",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "low": {
        "defaultValue": null,
        "description": "",
        "name": "low",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "manifest": {
        "defaultValue": null,
        "description": "",
        "name": "manifest",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "marginHeight": {
        "defaultValue": null,
        "description": "",
        "name": "marginHeight",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "marginWidth": {
        "defaultValue": null,
        "description": "",
        "name": "marginWidth",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "max": {
        "defaultValue": null,
        "description": "",
        "name": "max",
        "required": false,
        "type": {
          "name": "ReactText"
        }
      },
      "maxLength": {
        "defaultValue": null,
        "description": "",
        "name": "maxLength",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "media": {
        "defaultValue": null,
        "description": "",
        "name": "media",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "mediaGroup": {
        "defaultValue": null,
        "description": "",
        "name": "mediaGroup",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "method": {
        "defaultValue": null,
        "description": "",
        "name": "method",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "min": {
        "defaultValue": null,
        "description": "",
        "name": "min",
        "required": false,
        "type": {
          "name": "ReactText"
        }
      },
      "minLength": {
        "defaultValue": null,
        "description": "",
        "name": "minLength",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "multiple": {
        "defaultValue": null,
        "description": "",
        "name": "multiple",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "muted": {
        "defaultValue": null,
        "description": "",
        "name": "muted",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "name": {
        "defaultValue": null,
        "description": "",
        "name": "name",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "nonce": {
        "defaultValue": null,
        "description": "",
        "name": "nonce",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "noValidate": {
        "defaultValue": null,
        "description": "",
        "name": "noValidate",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "open": {
        "defaultValue": null,
        "description": "",
        "name": "open",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "optimum": {
        "defaultValue": null,
        "description": "",
        "name": "optimum",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "pattern": {
        "defaultValue": null,
        "description": "",
        "name": "pattern",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "placeholder": {
        "defaultValue": null,
        "description": "",
        "name": "placeholder",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "playsInline": {
        "defaultValue": null,
        "description": "",
        "name": "playsInline",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "poster": {
        "defaultValue": null,
        "description": "",
        "name": "poster",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "preload": {
        "defaultValue": null,
        "description": "",
        "name": "preload",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "readOnly": {
        "defaultValue": null,
        "description": "",
        "name": "readOnly",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "rel": {
        "defaultValue": null,
        "description": "",
        "name": "rel",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "required": {
        "defaultValue": null,
        "description": "",
        "name": "required",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "reversed": {
        "defaultValue": null,
        "description": "",
        "name": "reversed",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "rows": {
        "defaultValue": null,
        "description": "",
        "name": "rows",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "rowSpan": {
        "defaultValue": null,
        "description": "",
        "name": "rowSpan",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "sandbox": {
        "defaultValue": null,
        "description": "",
        "name": "sandbox",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "scope": {
        "defaultValue": null,
        "description": "",
        "name": "scope",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "scoped": {
        "defaultValue": null,
        "description": "",
        "name": "scoped",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "scrolling": {
        "defaultValue": null,
        "description": "",
        "name": "scrolling",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "seamless": {
        "defaultValue": null,
        "description": "",
        "name": "seamless",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "selected": {
        "defaultValue": null,
        "description": "",
        "name": "selected",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "shape": {
        "defaultValue": null,
        "description": "",
        "name": "shape",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "size": {
        "defaultValue": null,
        "description": "",
        "name": "size",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "sizes": {
        "defaultValue": null,
        "description": "",
        "name": "sizes",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "src": {
        "defaultValue": null,
        "description": "",
        "name": "src",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "srcDoc": {
        "defaultValue": null,
        "description": "",
        "name": "srcDoc",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "srcLang": {
        "defaultValue": null,
        "description": "",
        "name": "srcLang",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "srcSet": {
        "defaultValue": null,
        "description": "",
        "name": "srcSet",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "start": {
        "defaultValue": null,
        "description": "",
        "name": "start",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "step": {
        "defaultValue": null,
        "description": "",
        "name": "step",
        "required": false,
        "type": {
          "name": "ReactText"
        }
      },
      "target": {
        "defaultValue": null,
        "description": "",
        "name": "target",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "type": {
        "defaultValue": null,
        "description": "",
        "name": "type",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "useMap": {
        "defaultValue": null,
        "description": "",
        "name": "useMap",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "value": {
        "defaultValue": null,
        "description": "",
        "name": "value",
        "required": false,
        "type": {
          "name": "string | number | string[]"
        }
      },
      "width": {
        "defaultValue": null,
        "description": "",
        "name": "width",
        "required": false,
        "type": {
          "name": "ReactText"
        }
      },
      "wmode": {
        "defaultValue": null,
        "description": "",
        "name": "wmode",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "wrap": {
        "defaultValue": null,
        "description": "",
        "name": "wrap",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "defaultChecked": {
        "defaultValue": null,
        "description": "",
        "name": "defaultChecked",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "defaultValue": {
        "defaultValue": null,
        "description": "",
        "name": "defaultValue",
        "required": false,
        "type": {
          "name": "string | string[]"
        }
      },
      "suppressContentEditableWarning": {
        "defaultValue": null,
        "description": "",
        "name": "suppressContentEditableWarning",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "suppressHydrationWarning": {
        "defaultValue": null,
        "description": "",
        "name": "suppressHydrationWarning",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "accessKey": {
        "defaultValue": null,
        "description": "",
        "name": "accessKey",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "contentEditable": {
        "defaultValue": null,
        "description": "",
        "name": "contentEditable",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "contextMenu": {
        "defaultValue": null,
        "description": "",
        "name": "contextMenu",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "dir": {
        "defaultValue": null,
        "description": "",
        "name": "dir",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "draggable": {
        "defaultValue": null,
        "description": "",
        "name": "draggable",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "hidden": {
        "defaultValue": null,
        "description": "",
        "name": "hidden",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "id": {
        "defaultValue": null,
        "description": "",
        "name": "id",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "lang": {
        "defaultValue": null,
        "description": "",
        "name": "lang",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "slot": {
        "defaultValue": null,
        "description": "",
        "name": "slot",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "spellCheck": {
        "defaultValue": null,
        "description": "",
        "name": "spellCheck",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "tabIndex": {
        "defaultValue": null,
        "description": "",
        "name": "tabIndex",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "inputMode": {
        "defaultValue": null,
        "description": "",
        "name": "inputMode",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "is": {
        "defaultValue": null,
        "description": "",
        "name": "is",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "radioGroup": {
        "defaultValue": null,
        "description": "",
        "name": "radioGroup",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "role": {
        "defaultValue": null,
        "description": "",
        "name": "role",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "about": {
        "defaultValue": null,
        "description": "",
        "name": "about",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "datatype": {
        "defaultValue": null,
        "description": "",
        "name": "datatype",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "inlist": {
        "defaultValue": null,
        "description": "",
        "name": "inlist",
        "required": false,
        "type": {
          "name": "any"
        }
      },
      "prefix": {
        "defaultValue": null,
        "description": "",
        "name": "prefix",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "property": {
        "defaultValue": null,
        "description": "",
        "name": "property",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "resource": {
        "defaultValue": null,
        "description": "",
        "name": "resource",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "typeof": {
        "defaultValue": null,
        "description": "",
        "name": "typeof",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "vocab": {
        "defaultValue": null,
        "description": "",
        "name": "vocab",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "autoCapitalize": {
        "defaultValue": null,
        "description": "",
        "name": "autoCapitalize",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "autoCorrect": {
        "defaultValue": null,
        "description": "",
        "name": "autoCorrect",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "autoSave": {
        "defaultValue": null,
        "description": "",
        "name": "autoSave",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "color": {
        "defaultValue": null,
        "description": "",
        "name": "color",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "itemProp": {
        "defaultValue": null,
        "description": "",
        "name": "itemProp",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "itemScope": {
        "defaultValue": null,
        "description": "",
        "name": "itemScope",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "itemType": {
        "defaultValue": null,
        "description": "",
        "name": "itemType",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "itemID": {
        "defaultValue": null,
        "description": "",
        "name": "itemID",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "itemRef": {
        "defaultValue": null,
        "description": "",
        "name": "itemRef",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "results": {
        "defaultValue": null,
        "description": "",
        "name": "results",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "security": {
        "defaultValue": null,
        "description": "",
        "name": "security",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "unselectable": {
        "defaultValue": null,
        "description": "",
        "name": "unselectable",
        "required": false,
        "type": {
          "name": "\"on\" | \"off\""
        }
      },
      "aria-activedescendant": {
        "defaultValue": null,
        "description": "Identifies the currently active element when DOM focus is on a composite widget, textbox, group, or application.",
        "name": "aria-activedescendant",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-atomic": {
        "defaultValue": null,
        "description": "Indicates whether assistive technologies will present all, or only parts of, the changed region based on the change notifications defined by the aria-relevant attribute.",
        "name": "aria-atomic",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-autocomplete": {
        "defaultValue": null,
        "description": "Indicates whether inputting text could trigger display of one or more predictions of the user's intended value for an input and specifies how predictions would be\npresented if they are made.",
        "name": "aria-autocomplete",
        "required": false,
        "type": {
          "name": "\"list\" | \"none\" | \"inline\" | \"both\""
        }
      },
      "aria-busy": {
        "defaultValue": null,
        "description": "Indicates an element is being modified and that assistive technologies MAY want to wait until the modifications are complete before exposing them to the user.",
        "name": "aria-busy",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-checked": {
        "defaultValue": null,
        "description": "Indicates the current \"checked\" state of checkboxes, radio buttons, and other widgets.\n@see aria-pressed\n@see aria-selected.",
        "name": "aria-checked",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\" | \"mixed\""
        }
      },
      "aria-colcount": {
        "defaultValue": null,
        "description": "Defines the total number of columns in a table, grid, or treegrid.\n@see aria-colindex.",
        "name": "aria-colcount",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-colindex": {
        "defaultValue": null,
        "description": "Defines an element's column index or position with respect to the total number of columns within a table, grid, or treegrid.\n@see aria-colcount\n@see aria-colspan.",
        "name": "aria-colindex",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-colspan": {
        "defaultValue": null,
        "description": "Defines the number of columns spanned by a cell or gridcell within a table, grid, or treegrid.\n@see aria-colindex\n@see aria-rowspan.",
        "name": "aria-colspan",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-controls": {
        "defaultValue": null,
        "description": "Identifies the element (or elements) whose contents or presence are controlled by the current element.\n@see aria-owns.",
        "name": "aria-controls",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-current": {
        "defaultValue": null,
        "description": "Indicates the element that represents the current item within a container or set of related elements.",
        "name": "aria-current",
        "required": false,
        "type": {
          "name": "boolean | \"time\" | \"step\" | \"false\" | \"true\" | \"page\" | \"location\" | \"date\""
        }
      },
      "aria-describedby": {
        "defaultValue": null,
        "description": "Identifies the element (or elements) that describes the object.\n@see aria-labelledby",
        "name": "aria-describedby",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-details": {
        "defaultValue": null,
        "description": "Identifies the element that provides a detailed, extended description for the object.\n@see aria-describedby.",
        "name": "aria-details",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-disabled": {
        "defaultValue": null,
        "description": "Indicates that the element is perceivable but disabled, so it is not editable or otherwise operable.\n@see aria-hidden\n@see aria-readonly.",
        "name": "aria-disabled",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-dropeffect": {
        "defaultValue": null,
        "description": "Indicates what functions can be performed when a dragged object is released on the drop target.\n@deprecated in ARIA 1.1",
        "name": "aria-dropeffect",
        "required": false,
        "type": {
          "name": "\"link\" | \"none\" | \"copy\" | \"execute\" | \"move\" | \"popup\""
        }
      },
      "aria-errormessage": {
        "defaultValue": null,
        "description": "Identifies the element that provides an error message for the object.\n@see aria-invalid\n@see aria-describedby.",
        "name": "aria-errormessage",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-expanded": {
        "defaultValue": null,
        "description": "Indicates whether the element, or another grouping element it controls, is currently expanded or collapsed.",
        "name": "aria-expanded",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-flowto": {
        "defaultValue": null,
        "description": "Identifies the next element (or elements) in an alternate reading order of content which, at the user's discretion,\nallows assistive technology to override the general default of reading in document source order.",
        "name": "aria-flowto",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-grabbed": {
        "defaultValue": null,
        "description": "Indicates an element's \"grabbed\" state in a drag-and-drop operation.\n@deprecated in ARIA 1.1",
        "name": "aria-grabbed",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-haspopup": {
        "defaultValue": null,
        "description": "Indicates the availability and type of interactive popup element, such as menu or dialog, that can be triggered by an element.",
        "name": "aria-haspopup",
        "required": false,
        "type": {
          "name": "boolean | \"dialog\" | \"menu\" | \"false\" | \"true\" | \"listbox\" | \"tree\" | \"grid\""
        }
      },
      "aria-hidden": {
        "defaultValue": null,
        "description": "Indicates whether the element is exposed to an accessibility API.\n@see aria-disabled.",
        "name": "aria-hidden",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-invalid": {
        "defaultValue": null,
        "description": "Indicates the entered value does not conform to the format expected by the application.\n@see aria-errormessage.",
        "name": "aria-invalid",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\" | \"grammar\" | \"spelling\""
        }
      },
      "aria-keyshortcuts": {
        "defaultValue": null,
        "description": "Indicates keyboard shortcuts that an author has implemented to activate or give focus to an element.",
        "name": "aria-keyshortcuts",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-label": {
        "defaultValue": null,
        "description": "Defines a string value that labels the current element.\n@see aria-labelledby.",
        "name": "aria-label",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-labelledby": {
        "defaultValue": null,
        "description": "Identifies the element (or elements) that labels the current element.\n@see aria-describedby.",
        "name": "aria-labelledby",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-level": {
        "defaultValue": null,
        "description": "Defines the hierarchical level of an element within a structure.",
        "name": "aria-level",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-live": {
        "defaultValue": null,
        "description": "Indicates that an element will be updated, and describes the types of updates the user agents, assistive technologies, and user can expect from the live region.",
        "name": "aria-live",
        "required": false,
        "type": {
          "name": "\"off\" | \"assertive\" | \"polite\""
        }
      },
      "aria-modal": {
        "defaultValue": null,
        "description": "Indicates whether an element is modal when displayed.",
        "name": "aria-modal",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-multiline": {
        "defaultValue": null,
        "description": "Indicates whether a text box accepts multiple lines of input or only a single line.",
        "name": "aria-multiline",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-multiselectable": {
        "defaultValue": null,
        "description": "Indicates that the user may select more than one item from the current selectable descendants.",
        "name": "aria-multiselectable",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-orientation": {
        "defaultValue": null,
        "description": "Indicates whether the element's orientation is horizontal, vertical, or unknown/ambiguous.",
        "name": "aria-orientation",
        "required": false,
        "type": {
          "name": "\"horizontal\" | \"vertical\""
        }
      },
      "aria-owns": {
        "defaultValue": null,
        "description": "Identifies an element (or elements) in order to define a visual, functional, or contextual parent/child relationship\nbetween DOM elements where the DOM hierarchy cannot be used to represent the relationship.\n@see aria-controls.",
        "name": "aria-owns",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-placeholder": {
        "defaultValue": null,
        "description": "Defines a short hint (a word or short phrase) intended to aid the user with data entry when the control has no value.\nA hint could be a sample value or a brief description of the expected format.",
        "name": "aria-placeholder",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-posinset": {
        "defaultValue": null,
        "description": "Defines an element's number or position in the current set of listitems or treeitems. Not required if all elements in the set are present in the DOM.\n@see aria-setsize.",
        "name": "aria-posinset",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-pressed": {
        "defaultValue": null,
        "description": "Indicates the current \"pressed\" state of toggle buttons.\n@see aria-checked\n@see aria-selected.",
        "name": "aria-pressed",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\" | \"mixed\""
        }
      },
      "aria-readonly": {
        "defaultValue": null,
        "description": "Indicates that the element is not editable, but is otherwise operable.\n@see aria-disabled.",
        "name": "aria-readonly",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-relevant": {
        "defaultValue": null,
        "description": "Indicates what notifications the user agent will trigger when the accessibility tree within a live region is modified.\n@see aria-atomic.",
        "name": "aria-relevant",
        "required": false,
        "type": {
          "name": "\"additions\" | \"additions text\" | \"all\" | \"removals\" | \"text\""
        }
      },
      "aria-required": {
        "defaultValue": null,
        "description": "Indicates that user input is required on the element before a form may be submitted.",
        "name": "aria-required",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-roledescription": {
        "defaultValue": null,
        "description": "Defines a human-readable, author-localized description for the role of an element.",
        "name": "aria-roledescription",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-rowcount": {
        "defaultValue": null,
        "description": "Defines the total number of rows in a table, grid, or treegrid.\n@see aria-rowindex.",
        "name": "aria-rowcount",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-rowindex": {
        "defaultValue": null,
        "description": "Defines an element's row index or position with respect to the total number of rows within a table, grid, or treegrid.\n@see aria-rowcount\n@see aria-rowspan.",
        "name": "aria-rowindex",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-rowspan": {
        "defaultValue": null,
        "description": "Defines the number of rows spanned by a cell or gridcell within a table, grid, or treegrid.\n@see aria-rowindex\n@see aria-colspan.",
        "name": "aria-rowspan",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-selected": {
        "defaultValue": null,
        "description": "Indicates the current \"selected\" state of various widgets.\n@see aria-checked\n@see aria-pressed.",
        "name": "aria-selected",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-setsize": {
        "defaultValue": null,
        "description": "Defines the number of items in the current set of listitems or treeitems. Not required if all elements in the set are present in the DOM.\n@see aria-posinset.",
        "name": "aria-setsize",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-sort": {
        "defaultValue": null,
        "description": "Indicates if items in a table or grid are sorted in ascending or descending order.",
        "name": "aria-sort",
        "required": false,
        "type": {
          "name": "\"none\" | \"ascending\" | \"descending\" | \"other\""
        }
      },
      "aria-valuemax": {
        "defaultValue": null,
        "description": "Defines the maximum allowed value for a range widget.",
        "name": "aria-valuemax",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-valuemin": {
        "defaultValue": null,
        "description": "Defines the minimum allowed value for a range widget.",
        "name": "aria-valuemin",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-valuenow": {
        "defaultValue": null,
        "description": "Defines the current value for a range widget.\n@see aria-valuetext.",
        "name": "aria-valuenow",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-valuetext": {
        "defaultValue": null,
        "description": "Defines the human readable text alternative of aria-valuenow for a range widget.",
        "name": "aria-valuetext",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "dangerouslySetInnerHTML": {
        "defaultValue": null,
        "description": "",
        "name": "dangerouslySetInnerHTML",
        "required": false,
        "type": {
          "name": "{ __html: string; }"
        }
      },
      "onCopy": {
        "defaultValue": null,
        "description": "",
        "name": "onCopy",
        "required": false,
        "type": {
          "name": "(event: ClipboardEvent<HTMLElement>) => void"
        }
      },
      "onCopyCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onCopyCapture",
        "required": false,
        "type": {
          "name": "(event: ClipboardEvent<HTMLElement>) => void"
        }
      },
      "onCut": {
        "defaultValue": null,
        "description": "",
        "name": "onCut",
        "required": false,
        "type": {
          "name": "(event: ClipboardEvent<HTMLElement>) => void"
        }
      },
      "onCutCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onCutCapture",
        "required": false,
        "type": {
          "name": "(event: ClipboardEvent<HTMLElement>) => void"
        }
      },
      "onPaste": {
        "defaultValue": null,
        "description": "",
        "name": "onPaste",
        "required": false,
        "type": {
          "name": "(event: ClipboardEvent<HTMLElement>) => void"
        }
      },
      "onPasteCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPasteCapture",
        "required": false,
        "type": {
          "name": "(event: ClipboardEvent<HTMLElement>) => void"
        }
      },
      "onCompositionEnd": {
        "defaultValue": null,
        "description": "",
        "name": "onCompositionEnd",
        "required": false,
        "type": {
          "name": "(event: CompositionEvent<HTMLElement>) => void"
        }
      },
      "onCompositionEndCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onCompositionEndCapture",
        "required": false,
        "type": {
          "name": "(event: CompositionEvent<HTMLElement>) => void"
        }
      },
      "onCompositionStart": {
        "defaultValue": null,
        "description": "",
        "name": "onCompositionStart",
        "required": false,
        "type": {
          "name": "(event: CompositionEvent<HTMLElement>) => void"
        }
      },
      "onCompositionStartCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onCompositionStartCapture",
        "required": false,
        "type": {
          "name": "(event: CompositionEvent<HTMLElement>) => void"
        }
      },
      "onCompositionUpdate": {
        "defaultValue": null,
        "description": "",
        "name": "onCompositionUpdate",
        "required": false,
        "type": {
          "name": "(event: CompositionEvent<HTMLElement>) => void"
        }
      },
      "onCompositionUpdateCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onCompositionUpdateCapture",
        "required": false,
        "type": {
          "name": "(event: CompositionEvent<HTMLElement>) => void"
        }
      },
      "onFocus": {
        "defaultValue": null,
        "description": "",
        "name": "onFocus",
        "required": false,
        "type": {
          "name": "(event: FocusEvent<HTMLElement>) => void"
        }
      },
      "onFocusCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onFocusCapture",
        "required": false,
        "type": {
          "name": "(event: FocusEvent<HTMLElement>) => void"
        }
      },
      "onBlur": {
        "defaultValue": null,
        "description": "",
        "name": "onBlur",
        "required": false,
        "type": {
          "name": "(event: FocusEvent<HTMLElement>) => void"
        }
      },
      "onBlurCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onBlurCapture",
        "required": false,
        "type": {
          "name": "(event: FocusEvent<HTMLElement>) => void"
        }
      },
      "onChange": {
        "defaultValue": null,
        "description": "",
        "name": "onChange",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onChangeCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onChangeCapture",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onBeforeInput": {
        "defaultValue": null,
        "description": "",
        "name": "onBeforeInput",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onBeforeInputCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onBeforeInputCapture",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onInput": {
        "defaultValue": null,
        "description": "",
        "name": "onInput",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onInputCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onInputCapture",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onReset": {
        "defaultValue": null,
        "description": "",
        "name": "onReset",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onResetCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onResetCapture",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onSubmit": {
        "defaultValue": null,
        "description": "",
        "name": "onSubmit",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onSubmitCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onSubmitCapture",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onInvalid": {
        "defaultValue": null,
        "description": "",
        "name": "onInvalid",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onInvalidCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onInvalidCapture",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onLoad": {
        "defaultValue": null,
        "description": "",
        "name": "onLoad",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onLoadCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onLoadCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onError": {
        "defaultValue": null,
        "description": "",
        "name": "onError",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onErrorCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onErrorCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onKeyDown": {
        "defaultValue": null,
        "description": "",
        "name": "onKeyDown",
        "required": false,
        "type": {
          "name": "(event: KeyboardEvent<HTMLElement>) => void"
        }
      },
      "onKeyDownCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onKeyDownCapture",
        "required": false,
        "type": {
          "name": "(event: KeyboardEvent<HTMLElement>) => void"
        }
      },
      "onKeyPress": {
        "defaultValue": null,
        "description": "",
        "name": "onKeyPress",
        "required": false,
        "type": {
          "name": "(event: KeyboardEvent<HTMLElement>) => void"
        }
      },
      "onKeyPressCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onKeyPressCapture",
        "required": false,
        "type": {
          "name": "(event: KeyboardEvent<HTMLElement>) => void"
        }
      },
      "onKeyUp": {
        "defaultValue": null,
        "description": "",
        "name": "onKeyUp",
        "required": false,
        "type": {
          "name": "(event: KeyboardEvent<HTMLElement>) => void"
        }
      },
      "onKeyUpCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onKeyUpCapture",
        "required": false,
        "type": {
          "name": "(event: KeyboardEvent<HTMLElement>) => void"
        }
      },
      "onAbort": {
        "defaultValue": null,
        "description": "",
        "name": "onAbort",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onAbortCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onAbortCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onCanPlay": {
        "defaultValue": null,
        "description": "",
        "name": "onCanPlay",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onCanPlayCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onCanPlayCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onCanPlayThrough": {
        "defaultValue": null,
        "description": "",
        "name": "onCanPlayThrough",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onCanPlayThroughCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onCanPlayThroughCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onDurationChange": {
        "defaultValue": null,
        "description": "",
        "name": "onDurationChange",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onDurationChangeCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onDurationChangeCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onEmptied": {
        "defaultValue": null,
        "description": "",
        "name": "onEmptied",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onEmptiedCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onEmptiedCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onEncrypted": {
        "defaultValue": null,
        "description": "",
        "name": "onEncrypted",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onEncryptedCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onEncryptedCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onEnded": {
        "defaultValue": null,
        "description": "",
        "name": "onEnded",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onEndedCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onEndedCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onLoadedData": {
        "defaultValue": null,
        "description": "",
        "name": "onLoadedData",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onLoadedDataCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onLoadedDataCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onLoadedMetadata": {
        "defaultValue": null,
        "description": "",
        "name": "onLoadedMetadata",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onLoadedMetadataCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onLoadedMetadataCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onLoadStart": {
        "defaultValue": null,
        "description": "",
        "name": "onLoadStart",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onLoadStartCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onLoadStartCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onPause": {
        "defaultValue": null,
        "description": "",
        "name": "onPause",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onPauseCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPauseCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onPlay": {
        "defaultValue": null,
        "description": "",
        "name": "onPlay",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onPlayCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPlayCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onPlaying": {
        "defaultValue": null,
        "description": "",
        "name": "onPlaying",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onPlayingCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPlayingCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onProgress": {
        "defaultValue": null,
        "description": "",
        "name": "onProgress",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onProgressCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onProgressCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onRateChange": {
        "defaultValue": null,
        "description": "",
        "name": "onRateChange",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onRateChangeCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onRateChangeCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onSeeked": {
        "defaultValue": null,
        "description": "",
        "name": "onSeeked",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onSeekedCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onSeekedCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onSeeking": {
        "defaultValue": null,
        "description": "",
        "name": "onSeeking",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onSeekingCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onSeekingCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onStalled": {
        "defaultValue": null,
        "description": "",
        "name": "onStalled",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onStalledCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onStalledCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onSuspend": {
        "defaultValue": null,
        "description": "",
        "name": "onSuspend",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onSuspendCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onSuspendCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onTimeUpdate": {
        "defaultValue": null,
        "description": "",
        "name": "onTimeUpdate",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onTimeUpdateCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onTimeUpdateCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onVolumeChange": {
        "defaultValue": null,
        "description": "",
        "name": "onVolumeChange",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onVolumeChangeCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onVolumeChangeCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onWaiting": {
        "defaultValue": null,
        "description": "",
        "name": "onWaiting",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onWaitingCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onWaitingCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onAuxClick": {
        "defaultValue": null,
        "description": "",
        "name": "onAuxClick",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onAuxClickCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onAuxClickCapture",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onClick": {
        "defaultValue": null,
        "description": "",
        "name": "onClick",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onClickCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onClickCapture",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onContextMenu": {
        "defaultValue": null,
        "description": "",
        "name": "onContextMenu",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onContextMenuCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onContextMenuCapture",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onDoubleClick": {
        "defaultValue": null,
        "description": "",
        "name": "onDoubleClick",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onDoubleClickCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onDoubleClickCapture",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onDrag": {
        "defaultValue": null,
        "description": "",
        "name": "onDrag",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onDragCapture",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragEnd": {
        "defaultValue": null,
        "description": "",
        "name": "onDragEnd",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragEndCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onDragEndCapture",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragEnter": {
        "defaultValue": null,
        "description": "",
        "name": "onDragEnter",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragEnterCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onDragEnterCapture",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragExit": {
        "defaultValue": null,
        "description": "",
        "name": "onDragExit",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragExitCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onDragExitCapture",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragLeave": {
        "defaultValue": null,
        "description": "",
        "name": "onDragLeave",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragLeaveCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onDragLeaveCapture",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragOver": {
        "defaultValue": null,
        "description": "",
        "name": "onDragOver",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragOverCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onDragOverCapture",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragStart": {
        "defaultValue": null,
        "description": "",
        "name": "onDragStart",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragStartCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onDragStartCapture",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDrop": {
        "defaultValue": null,
        "description": "",
        "name": "onDrop",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDropCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onDropCapture",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onMouseDown": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseDown",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onMouseDownCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseDownCapture",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onMouseEnter": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseEnter",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onMouseLeave": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseLeave",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onMouseMove": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseMove",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onMouseMoveCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseMoveCapture",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onMouseOut": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseOut",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onMouseOutCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseOutCapture",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onMouseOver": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseOver",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onMouseOverCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseOverCapture",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onMouseUp": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseUp",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onMouseUpCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseUpCapture",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onSelect": {
        "defaultValue": null,
        "description": "",
        "name": "onSelect",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onSelectCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onSelectCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onTouchCancel": {
        "defaultValue": null,
        "description": "",
        "name": "onTouchCancel",
        "required": false,
        "type": {
          "name": "(event: TouchEvent<HTMLElement>) => void"
        }
      },
      "onTouchCancelCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onTouchCancelCapture",
        "required": false,
        "type": {
          "name": "(event: TouchEvent<HTMLElement>) => void"
        }
      },
      "onTouchEnd": {
        "defaultValue": null,
        "description": "",
        "name": "onTouchEnd",
        "required": false,
        "type": {
          "name": "(event: TouchEvent<HTMLElement>) => void"
        }
      },
      "onTouchEndCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onTouchEndCapture",
        "required": false,
        "type": {
          "name": "(event: TouchEvent<HTMLElement>) => void"
        }
      },
      "onTouchMove": {
        "defaultValue": null,
        "description": "",
        "name": "onTouchMove",
        "required": false,
        "type": {
          "name": "(event: TouchEvent<HTMLElement>) => void"
        }
      },
      "onTouchMoveCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onTouchMoveCapture",
        "required": false,
        "type": {
          "name": "(event: TouchEvent<HTMLElement>) => void"
        }
      },
      "onTouchStart": {
        "defaultValue": null,
        "description": "",
        "name": "onTouchStart",
        "required": false,
        "type": {
          "name": "(event: TouchEvent<HTMLElement>) => void"
        }
      },
      "onTouchStartCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onTouchStartCapture",
        "required": false,
        "type": {
          "name": "(event: TouchEvent<HTMLElement>) => void"
        }
      },
      "onPointerDown": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerDown",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerDownCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerDownCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerMove": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerMove",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerMoveCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerMoveCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerUp": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerUp",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerUpCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerUpCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerCancel": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerCancel",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerCancelCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerCancelCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerEnter": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerEnter",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerEnterCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerEnterCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerLeave": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerLeave",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerLeaveCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerLeaveCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerOver": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerOver",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerOverCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerOverCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerOut": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerOut",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerOutCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerOutCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onGotPointerCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onGotPointerCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onGotPointerCaptureCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onGotPointerCaptureCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onLostPointerCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onLostPointerCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onLostPointerCaptureCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onLostPointerCaptureCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onScroll": {
        "defaultValue": null,
        "description": "",
        "name": "onScroll",
        "required": false,
        "type": {
          "name": "(event: UIEvent<HTMLElement>) => void"
        }
      },
      "onScrollCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onScrollCapture",
        "required": false,
        "type": {
          "name": "(event: UIEvent<HTMLElement>) => void"
        }
      },
      "onWheel": {
        "defaultValue": null,
        "description": "",
        "name": "onWheel",
        "required": false,
        "type": {
          "name": "(event: WheelEvent<HTMLElement>) => void"
        }
      },
      "onWheelCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onWheelCapture",
        "required": false,
        "type": {
          "name": "(event: WheelEvent<HTMLElement>) => void"
        }
      },
      "onAnimationStart": {
        "defaultValue": null,
        "description": "",
        "name": "onAnimationStart",
        "required": false,
        "type": {
          "name": "(event: AnimationEvent<HTMLElement>) => void"
        }
      },
      "onAnimationStartCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onAnimationStartCapture",
        "required": false,
        "type": {
          "name": "(event: AnimationEvent<HTMLElement>) => void"
        }
      },
      "onAnimationEnd": {
        "defaultValue": null,
        "description": "",
        "name": "onAnimationEnd",
        "required": false,
        "type": {
          "name": "(event: AnimationEvent<HTMLElement>) => void"
        }
      },
      "onAnimationEndCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onAnimationEndCapture",
        "required": false,
        "type": {
          "name": "(event: AnimationEvent<HTMLElement>) => void"
        }
      },
      "onAnimationIteration": {
        "defaultValue": null,
        "description": "",
        "name": "onAnimationIteration",
        "required": false,
        "type": {
          "name": "(event: AnimationEvent<HTMLElement>) => void"
        }
      },
      "onAnimationIterationCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onAnimationIterationCapture",
        "required": false,
        "type": {
          "name": "(event: AnimationEvent<HTMLElement>) => void"
        }
      },
      "onTransitionEnd": {
        "defaultValue": null,
        "description": "",
        "name": "onTransitionEnd",
        "required": false,
        "type": {
          "name": "(event: TransitionEvent<HTMLElement>) => void"
        }
      },
      "onTransitionEndCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onTransitionEndCapture",
        "required": false,
        "type": {
          "name": "(event: TransitionEvent<HTMLElement>) => void"
        }
      },
      "key": {
        "defaultValue": null,
        "description": "",
        "name": "key",
        "required": false,
        "type": {
          "name": "ReactText"
        }
      },
      "ref": {
        "defaultValue": null,
        "description": "",
        "name": "ref",
        "required": false,
        "type": {
          "name": "Ref<HTMLElement>"
        }
      }
    }
  }; // @ts-ignore

  if (typeof STORYBOOK_REACT_CLASSES !== "undefined") // @ts-ignore
    STORYBOOK_REACT_CLASSES["components/Button/index.tsx#ButtonLink"] = {
      docgenInfo: ButtonLink.__docgenInfo,
      name: "ButtonLink",
      path: "components/Button/index.tsx#ButtonLink"
    };
} catch (__react_docgen_typescript_loader_error) {}

try {
  // @ts-ignore
  Button.displayName = "Button"; // @ts-ignore

  Button.__docgenInfo = {
    "description": "",
    "displayName": "Button",
    "props": {
      "cite": {
        "defaultValue": null,
        "description": "",
        "name": "cite",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "data": {
        "defaultValue": null,
        "description": "",
        "name": "data",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "form": {
        "defaultValue": null,
        "description": "",
        "name": "form",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "label": {
        "defaultValue": null,
        "description": "",
        "name": "label",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "span": {
        "defaultValue": null,
        "description": "",
        "name": "span",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "style": {
        "defaultValue": null,
        "description": "",
        "name": "style",
        "required": false,
        "type": {
          "name": "CSSProperties"
        }
      },
      "summary": {
        "defaultValue": null,
        "description": "",
        "name": "summary",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "title": {
        "defaultValue": null,
        "description": "",
        "name": "title",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "className": {
        "defaultValue": null,
        "description": "",
        "name": "className",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "accept": {
        "defaultValue": null,
        "description": "",
        "name": "accept",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "acceptCharset": {
        "defaultValue": null,
        "description": "",
        "name": "acceptCharset",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "action": {
        "defaultValue": null,
        "description": "",
        "name": "action",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "allowFullScreen": {
        "defaultValue": null,
        "description": "",
        "name": "allowFullScreen",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "allowTransparency": {
        "defaultValue": null,
        "description": "",
        "name": "allowTransparency",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "alt": {
        "defaultValue": null,
        "description": "",
        "name": "alt",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "as": {
        "defaultValue": null,
        "description": "",
        "name": "as",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "async": {
        "defaultValue": null,
        "description": "",
        "name": "async",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "autoComplete": {
        "defaultValue": null,
        "description": "",
        "name": "autoComplete",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "autoFocus": {
        "defaultValue": null,
        "description": "",
        "name": "autoFocus",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "autoPlay": {
        "defaultValue": null,
        "description": "",
        "name": "autoPlay",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "capture": {
        "defaultValue": null,
        "description": "",
        "name": "capture",
        "required": false,
        "type": {
          "name": "string | boolean"
        }
      },
      "cellPadding": {
        "defaultValue": null,
        "description": "",
        "name": "cellPadding",
        "required": false,
        "type": {
          "name": "ReactText"
        }
      },
      "cellSpacing": {
        "defaultValue": null,
        "description": "",
        "name": "cellSpacing",
        "required": false,
        "type": {
          "name": "ReactText"
        }
      },
      "charSet": {
        "defaultValue": null,
        "description": "",
        "name": "charSet",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "challenge": {
        "defaultValue": null,
        "description": "",
        "name": "challenge",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "checked": {
        "defaultValue": null,
        "description": "",
        "name": "checked",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "classID": {
        "defaultValue": null,
        "description": "",
        "name": "classID",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "cols": {
        "defaultValue": null,
        "description": "",
        "name": "cols",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "colSpan": {
        "defaultValue": null,
        "description": "",
        "name": "colSpan",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "content": {
        "defaultValue": null,
        "description": "",
        "name": "content",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "controls": {
        "defaultValue": null,
        "description": "",
        "name": "controls",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "coords": {
        "defaultValue": null,
        "description": "",
        "name": "coords",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "crossOrigin": {
        "defaultValue": null,
        "description": "",
        "name": "crossOrigin",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "dateTime": {
        "defaultValue": null,
        "description": "",
        "name": "dateTime",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "default": {
        "defaultValue": null,
        "description": "",
        "name": "default",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "defer": {
        "defaultValue": null,
        "description": "",
        "name": "defer",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "disabled": {
        "defaultValue": null,
        "description": "",
        "name": "disabled",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "download": {
        "defaultValue": null,
        "description": "",
        "name": "download",
        "required": false,
        "type": {
          "name": "any"
        }
      },
      "encType": {
        "defaultValue": null,
        "description": "",
        "name": "encType",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "formAction": {
        "defaultValue": null,
        "description": "",
        "name": "formAction",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "formEncType": {
        "defaultValue": null,
        "description": "",
        "name": "formEncType",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "formMethod": {
        "defaultValue": null,
        "description": "",
        "name": "formMethod",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "formNoValidate": {
        "defaultValue": null,
        "description": "",
        "name": "formNoValidate",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "formTarget": {
        "defaultValue": null,
        "description": "",
        "name": "formTarget",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "frameBorder": {
        "defaultValue": null,
        "description": "",
        "name": "frameBorder",
        "required": false,
        "type": {
          "name": "ReactText"
        }
      },
      "headers": {
        "defaultValue": null,
        "description": "",
        "name": "headers",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "height": {
        "defaultValue": null,
        "description": "",
        "name": "height",
        "required": false,
        "type": {
          "name": "ReactText"
        }
      },
      "high": {
        "defaultValue": null,
        "description": "",
        "name": "high",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "href": {
        "defaultValue": null,
        "description": "",
        "name": "href",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "hrefLang": {
        "defaultValue": null,
        "description": "",
        "name": "hrefLang",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "htmlFor": {
        "defaultValue": null,
        "description": "",
        "name": "htmlFor",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "httpEquiv": {
        "defaultValue": null,
        "description": "",
        "name": "httpEquiv",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "integrity": {
        "defaultValue": null,
        "description": "",
        "name": "integrity",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "keyParams": {
        "defaultValue": null,
        "description": "",
        "name": "keyParams",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "keyType": {
        "defaultValue": null,
        "description": "",
        "name": "keyType",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "kind": {
        "defaultValue": null,
        "description": "",
        "name": "kind",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "list": {
        "defaultValue": null,
        "description": "",
        "name": "list",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "loop": {
        "defaultValue": null,
        "description": "",
        "name": "loop",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "low": {
        "defaultValue": null,
        "description": "",
        "name": "low",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "manifest": {
        "defaultValue": null,
        "description": "",
        "name": "manifest",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "marginHeight": {
        "defaultValue": null,
        "description": "",
        "name": "marginHeight",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "marginWidth": {
        "defaultValue": null,
        "description": "",
        "name": "marginWidth",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "max": {
        "defaultValue": null,
        "description": "",
        "name": "max",
        "required": false,
        "type": {
          "name": "ReactText"
        }
      },
      "maxLength": {
        "defaultValue": null,
        "description": "",
        "name": "maxLength",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "media": {
        "defaultValue": null,
        "description": "",
        "name": "media",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "mediaGroup": {
        "defaultValue": null,
        "description": "",
        "name": "mediaGroup",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "method": {
        "defaultValue": null,
        "description": "",
        "name": "method",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "min": {
        "defaultValue": null,
        "description": "",
        "name": "min",
        "required": false,
        "type": {
          "name": "ReactText"
        }
      },
      "minLength": {
        "defaultValue": null,
        "description": "",
        "name": "minLength",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "multiple": {
        "defaultValue": null,
        "description": "",
        "name": "multiple",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "muted": {
        "defaultValue": null,
        "description": "",
        "name": "muted",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "name": {
        "defaultValue": null,
        "description": "",
        "name": "name",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "nonce": {
        "defaultValue": null,
        "description": "",
        "name": "nonce",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "noValidate": {
        "defaultValue": null,
        "description": "",
        "name": "noValidate",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "open": {
        "defaultValue": null,
        "description": "",
        "name": "open",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "optimum": {
        "defaultValue": null,
        "description": "",
        "name": "optimum",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "pattern": {
        "defaultValue": null,
        "description": "",
        "name": "pattern",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "placeholder": {
        "defaultValue": null,
        "description": "",
        "name": "placeholder",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "playsInline": {
        "defaultValue": null,
        "description": "",
        "name": "playsInline",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "poster": {
        "defaultValue": null,
        "description": "",
        "name": "poster",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "preload": {
        "defaultValue": null,
        "description": "",
        "name": "preload",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "readOnly": {
        "defaultValue": null,
        "description": "",
        "name": "readOnly",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "rel": {
        "defaultValue": null,
        "description": "",
        "name": "rel",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "required": {
        "defaultValue": null,
        "description": "",
        "name": "required",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "reversed": {
        "defaultValue": null,
        "description": "",
        "name": "reversed",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "rows": {
        "defaultValue": null,
        "description": "",
        "name": "rows",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "rowSpan": {
        "defaultValue": null,
        "description": "",
        "name": "rowSpan",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "sandbox": {
        "defaultValue": null,
        "description": "",
        "name": "sandbox",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "scope": {
        "defaultValue": null,
        "description": "",
        "name": "scope",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "scoped": {
        "defaultValue": null,
        "description": "",
        "name": "scoped",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "scrolling": {
        "defaultValue": null,
        "description": "",
        "name": "scrolling",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "seamless": {
        "defaultValue": null,
        "description": "",
        "name": "seamless",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "selected": {
        "defaultValue": null,
        "description": "",
        "name": "selected",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "shape": {
        "defaultValue": null,
        "description": "",
        "name": "shape",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "size": {
        "defaultValue": null,
        "description": "",
        "name": "size",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "sizes": {
        "defaultValue": null,
        "description": "",
        "name": "sizes",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "src": {
        "defaultValue": null,
        "description": "",
        "name": "src",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "srcDoc": {
        "defaultValue": null,
        "description": "",
        "name": "srcDoc",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "srcLang": {
        "defaultValue": null,
        "description": "",
        "name": "srcLang",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "srcSet": {
        "defaultValue": null,
        "description": "",
        "name": "srcSet",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "start": {
        "defaultValue": null,
        "description": "",
        "name": "start",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "step": {
        "defaultValue": null,
        "description": "",
        "name": "step",
        "required": false,
        "type": {
          "name": "ReactText"
        }
      },
      "target": {
        "defaultValue": null,
        "description": "",
        "name": "target",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "type": {
        "defaultValue": null,
        "description": "",
        "name": "type",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "useMap": {
        "defaultValue": null,
        "description": "",
        "name": "useMap",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "value": {
        "defaultValue": null,
        "description": "",
        "name": "value",
        "required": false,
        "type": {
          "name": "string | number | string[]"
        }
      },
      "width": {
        "defaultValue": null,
        "description": "",
        "name": "width",
        "required": false,
        "type": {
          "name": "ReactText"
        }
      },
      "wmode": {
        "defaultValue": null,
        "description": "",
        "name": "wmode",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "wrap": {
        "defaultValue": null,
        "description": "",
        "name": "wrap",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "defaultChecked": {
        "defaultValue": null,
        "description": "",
        "name": "defaultChecked",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "defaultValue": {
        "defaultValue": null,
        "description": "",
        "name": "defaultValue",
        "required": false,
        "type": {
          "name": "string | string[]"
        }
      },
      "suppressContentEditableWarning": {
        "defaultValue": null,
        "description": "",
        "name": "suppressContentEditableWarning",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "suppressHydrationWarning": {
        "defaultValue": null,
        "description": "",
        "name": "suppressHydrationWarning",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "accessKey": {
        "defaultValue": null,
        "description": "",
        "name": "accessKey",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "contentEditable": {
        "defaultValue": null,
        "description": "",
        "name": "contentEditable",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "contextMenu": {
        "defaultValue": null,
        "description": "",
        "name": "contextMenu",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "dir": {
        "defaultValue": null,
        "description": "",
        "name": "dir",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "draggable": {
        "defaultValue": null,
        "description": "",
        "name": "draggable",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "hidden": {
        "defaultValue": null,
        "description": "",
        "name": "hidden",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "id": {
        "defaultValue": null,
        "description": "",
        "name": "id",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "lang": {
        "defaultValue": null,
        "description": "",
        "name": "lang",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "slot": {
        "defaultValue": null,
        "description": "",
        "name": "slot",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "spellCheck": {
        "defaultValue": null,
        "description": "",
        "name": "spellCheck",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "tabIndex": {
        "defaultValue": null,
        "description": "",
        "name": "tabIndex",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "inputMode": {
        "defaultValue": null,
        "description": "",
        "name": "inputMode",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "is": {
        "defaultValue": null,
        "description": "",
        "name": "is",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "radioGroup": {
        "defaultValue": null,
        "description": "",
        "name": "radioGroup",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "role": {
        "defaultValue": null,
        "description": "",
        "name": "role",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "about": {
        "defaultValue": null,
        "description": "",
        "name": "about",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "datatype": {
        "defaultValue": null,
        "description": "",
        "name": "datatype",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "inlist": {
        "defaultValue": null,
        "description": "",
        "name": "inlist",
        "required": false,
        "type": {
          "name": "any"
        }
      },
      "prefix": {
        "defaultValue": null,
        "description": "",
        "name": "prefix",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "property": {
        "defaultValue": null,
        "description": "",
        "name": "property",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "resource": {
        "defaultValue": null,
        "description": "",
        "name": "resource",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "typeof": {
        "defaultValue": null,
        "description": "",
        "name": "typeof",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "vocab": {
        "defaultValue": null,
        "description": "",
        "name": "vocab",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "autoCapitalize": {
        "defaultValue": null,
        "description": "",
        "name": "autoCapitalize",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "autoCorrect": {
        "defaultValue": null,
        "description": "",
        "name": "autoCorrect",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "autoSave": {
        "defaultValue": null,
        "description": "",
        "name": "autoSave",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "color": {
        "defaultValue": null,
        "description": "",
        "name": "color",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "itemProp": {
        "defaultValue": null,
        "description": "",
        "name": "itemProp",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "itemScope": {
        "defaultValue": null,
        "description": "",
        "name": "itemScope",
        "required": false,
        "type": {
          "name": "boolean"
        }
      },
      "itemType": {
        "defaultValue": null,
        "description": "",
        "name": "itemType",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "itemID": {
        "defaultValue": null,
        "description": "",
        "name": "itemID",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "itemRef": {
        "defaultValue": null,
        "description": "",
        "name": "itemRef",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "results": {
        "defaultValue": null,
        "description": "",
        "name": "results",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "security": {
        "defaultValue": null,
        "description": "",
        "name": "security",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "unselectable": {
        "defaultValue": null,
        "description": "",
        "name": "unselectable",
        "required": false,
        "type": {
          "name": "\"on\" | \"off\""
        }
      },
      "aria-activedescendant": {
        "defaultValue": null,
        "description": "Identifies the currently active element when DOM focus is on a composite widget, textbox, group, or application.",
        "name": "aria-activedescendant",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-atomic": {
        "defaultValue": null,
        "description": "Indicates whether assistive technologies will present all, or only parts of, the changed region based on the change notifications defined by the aria-relevant attribute.",
        "name": "aria-atomic",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-autocomplete": {
        "defaultValue": null,
        "description": "Indicates whether inputting text could trigger display of one or more predictions of the user's intended value for an input and specifies how predictions would be\npresented if they are made.",
        "name": "aria-autocomplete",
        "required": false,
        "type": {
          "name": "\"list\" | \"none\" | \"inline\" | \"both\""
        }
      },
      "aria-busy": {
        "defaultValue": null,
        "description": "Indicates an element is being modified and that assistive technologies MAY want to wait until the modifications are complete before exposing them to the user.",
        "name": "aria-busy",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-checked": {
        "defaultValue": null,
        "description": "Indicates the current \"checked\" state of checkboxes, radio buttons, and other widgets.\n@see aria-pressed\n@see aria-selected.",
        "name": "aria-checked",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\" | \"mixed\""
        }
      },
      "aria-colcount": {
        "defaultValue": null,
        "description": "Defines the total number of columns in a table, grid, or treegrid.\n@see aria-colindex.",
        "name": "aria-colcount",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-colindex": {
        "defaultValue": null,
        "description": "Defines an element's column index or position with respect to the total number of columns within a table, grid, or treegrid.\n@see aria-colcount\n@see aria-colspan.",
        "name": "aria-colindex",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-colspan": {
        "defaultValue": null,
        "description": "Defines the number of columns spanned by a cell or gridcell within a table, grid, or treegrid.\n@see aria-colindex\n@see aria-rowspan.",
        "name": "aria-colspan",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-controls": {
        "defaultValue": null,
        "description": "Identifies the element (or elements) whose contents or presence are controlled by the current element.\n@see aria-owns.",
        "name": "aria-controls",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-current": {
        "defaultValue": null,
        "description": "Indicates the element that represents the current item within a container or set of related elements.",
        "name": "aria-current",
        "required": false,
        "type": {
          "name": "boolean | \"time\" | \"step\" | \"false\" | \"true\" | \"page\" | \"location\" | \"date\""
        }
      },
      "aria-describedby": {
        "defaultValue": null,
        "description": "Identifies the element (or elements) that describes the object.\n@see aria-labelledby",
        "name": "aria-describedby",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-details": {
        "defaultValue": null,
        "description": "Identifies the element that provides a detailed, extended description for the object.\n@see aria-describedby.",
        "name": "aria-details",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-disabled": {
        "defaultValue": null,
        "description": "Indicates that the element is perceivable but disabled, so it is not editable or otherwise operable.\n@see aria-hidden\n@see aria-readonly.",
        "name": "aria-disabled",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-dropeffect": {
        "defaultValue": null,
        "description": "Indicates what functions can be performed when a dragged object is released on the drop target.\n@deprecated in ARIA 1.1",
        "name": "aria-dropeffect",
        "required": false,
        "type": {
          "name": "\"link\" | \"none\" | \"copy\" | \"execute\" | \"move\" | \"popup\""
        }
      },
      "aria-errormessage": {
        "defaultValue": null,
        "description": "Identifies the element that provides an error message for the object.\n@see aria-invalid\n@see aria-describedby.",
        "name": "aria-errormessage",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-expanded": {
        "defaultValue": null,
        "description": "Indicates whether the element, or another grouping element it controls, is currently expanded or collapsed.",
        "name": "aria-expanded",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-flowto": {
        "defaultValue": null,
        "description": "Identifies the next element (or elements) in an alternate reading order of content which, at the user's discretion,\nallows assistive technology to override the general default of reading in document source order.",
        "name": "aria-flowto",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-grabbed": {
        "defaultValue": null,
        "description": "Indicates an element's \"grabbed\" state in a drag-and-drop operation.\n@deprecated in ARIA 1.1",
        "name": "aria-grabbed",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-haspopup": {
        "defaultValue": null,
        "description": "Indicates the availability and type of interactive popup element, such as menu or dialog, that can be triggered by an element.",
        "name": "aria-haspopup",
        "required": false,
        "type": {
          "name": "boolean | \"dialog\" | \"menu\" | \"false\" | \"true\" | \"listbox\" | \"tree\" | \"grid\""
        }
      },
      "aria-hidden": {
        "defaultValue": null,
        "description": "Indicates whether the element is exposed to an accessibility API.\n@see aria-disabled.",
        "name": "aria-hidden",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-invalid": {
        "defaultValue": null,
        "description": "Indicates the entered value does not conform to the format expected by the application.\n@see aria-errormessage.",
        "name": "aria-invalid",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\" | \"grammar\" | \"spelling\""
        }
      },
      "aria-keyshortcuts": {
        "defaultValue": null,
        "description": "Indicates keyboard shortcuts that an author has implemented to activate or give focus to an element.",
        "name": "aria-keyshortcuts",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-label": {
        "defaultValue": null,
        "description": "Defines a string value that labels the current element.\n@see aria-labelledby.",
        "name": "aria-label",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-labelledby": {
        "defaultValue": null,
        "description": "Identifies the element (or elements) that labels the current element.\n@see aria-describedby.",
        "name": "aria-labelledby",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-level": {
        "defaultValue": null,
        "description": "Defines the hierarchical level of an element within a structure.",
        "name": "aria-level",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-live": {
        "defaultValue": null,
        "description": "Indicates that an element will be updated, and describes the types of updates the user agents, assistive technologies, and user can expect from the live region.",
        "name": "aria-live",
        "required": false,
        "type": {
          "name": "\"off\" | \"assertive\" | \"polite\""
        }
      },
      "aria-modal": {
        "defaultValue": null,
        "description": "Indicates whether an element is modal when displayed.",
        "name": "aria-modal",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-multiline": {
        "defaultValue": null,
        "description": "Indicates whether a text box accepts multiple lines of input or only a single line.",
        "name": "aria-multiline",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-multiselectable": {
        "defaultValue": null,
        "description": "Indicates that the user may select more than one item from the current selectable descendants.",
        "name": "aria-multiselectable",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-orientation": {
        "defaultValue": null,
        "description": "Indicates whether the element's orientation is horizontal, vertical, or unknown/ambiguous.",
        "name": "aria-orientation",
        "required": false,
        "type": {
          "name": "\"horizontal\" | \"vertical\""
        }
      },
      "aria-owns": {
        "defaultValue": null,
        "description": "Identifies an element (or elements) in order to define a visual, functional, or contextual parent/child relationship\nbetween DOM elements where the DOM hierarchy cannot be used to represent the relationship.\n@see aria-controls.",
        "name": "aria-owns",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-placeholder": {
        "defaultValue": null,
        "description": "Defines a short hint (a word or short phrase) intended to aid the user with data entry when the control has no value.\nA hint could be a sample value or a brief description of the expected format.",
        "name": "aria-placeholder",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-posinset": {
        "defaultValue": null,
        "description": "Defines an element's number or position in the current set of listitems or treeitems. Not required if all elements in the set are present in the DOM.\n@see aria-setsize.",
        "name": "aria-posinset",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-pressed": {
        "defaultValue": null,
        "description": "Indicates the current \"pressed\" state of toggle buttons.\n@see aria-checked\n@see aria-selected.",
        "name": "aria-pressed",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\" | \"mixed\""
        }
      },
      "aria-readonly": {
        "defaultValue": null,
        "description": "Indicates that the element is not editable, but is otherwise operable.\n@see aria-disabled.",
        "name": "aria-readonly",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-relevant": {
        "defaultValue": null,
        "description": "Indicates what notifications the user agent will trigger when the accessibility tree within a live region is modified.\n@see aria-atomic.",
        "name": "aria-relevant",
        "required": false,
        "type": {
          "name": "\"additions\" | \"additions text\" | \"all\" | \"removals\" | \"text\""
        }
      },
      "aria-required": {
        "defaultValue": null,
        "description": "Indicates that user input is required on the element before a form may be submitted.",
        "name": "aria-required",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-roledescription": {
        "defaultValue": null,
        "description": "Defines a human-readable, author-localized description for the role of an element.",
        "name": "aria-roledescription",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "aria-rowcount": {
        "defaultValue": null,
        "description": "Defines the total number of rows in a table, grid, or treegrid.\n@see aria-rowindex.",
        "name": "aria-rowcount",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-rowindex": {
        "defaultValue": null,
        "description": "Defines an element's row index or position with respect to the total number of rows within a table, grid, or treegrid.\n@see aria-rowcount\n@see aria-rowspan.",
        "name": "aria-rowindex",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-rowspan": {
        "defaultValue": null,
        "description": "Defines the number of rows spanned by a cell or gridcell within a table, grid, or treegrid.\n@see aria-rowindex\n@see aria-colspan.",
        "name": "aria-rowspan",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-selected": {
        "defaultValue": null,
        "description": "Indicates the current \"selected\" state of various widgets.\n@see aria-checked\n@see aria-pressed.",
        "name": "aria-selected",
        "required": false,
        "type": {
          "name": "boolean | \"false\" | \"true\""
        }
      },
      "aria-setsize": {
        "defaultValue": null,
        "description": "Defines the number of items in the current set of listitems or treeitems. Not required if all elements in the set are present in the DOM.\n@see aria-posinset.",
        "name": "aria-setsize",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-sort": {
        "defaultValue": null,
        "description": "Indicates if items in a table or grid are sorted in ascending or descending order.",
        "name": "aria-sort",
        "required": false,
        "type": {
          "name": "\"none\" | \"ascending\" | \"descending\" | \"other\""
        }
      },
      "aria-valuemax": {
        "defaultValue": null,
        "description": "Defines the maximum allowed value for a range widget.",
        "name": "aria-valuemax",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-valuemin": {
        "defaultValue": null,
        "description": "Defines the minimum allowed value for a range widget.",
        "name": "aria-valuemin",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-valuenow": {
        "defaultValue": null,
        "description": "Defines the current value for a range widget.\n@see aria-valuetext.",
        "name": "aria-valuenow",
        "required": false,
        "type": {
          "name": "number"
        }
      },
      "aria-valuetext": {
        "defaultValue": null,
        "description": "Defines the human readable text alternative of aria-valuenow for a range widget.",
        "name": "aria-valuetext",
        "required": false,
        "type": {
          "name": "string"
        }
      },
      "dangerouslySetInnerHTML": {
        "defaultValue": null,
        "description": "",
        "name": "dangerouslySetInnerHTML",
        "required": false,
        "type": {
          "name": "{ __html: string; }"
        }
      },
      "onCopy": {
        "defaultValue": null,
        "description": "",
        "name": "onCopy",
        "required": false,
        "type": {
          "name": "(event: ClipboardEvent<HTMLElement>) => void"
        }
      },
      "onCopyCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onCopyCapture",
        "required": false,
        "type": {
          "name": "(event: ClipboardEvent<HTMLElement>) => void"
        }
      },
      "onCut": {
        "defaultValue": null,
        "description": "",
        "name": "onCut",
        "required": false,
        "type": {
          "name": "(event: ClipboardEvent<HTMLElement>) => void"
        }
      },
      "onCutCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onCutCapture",
        "required": false,
        "type": {
          "name": "(event: ClipboardEvent<HTMLElement>) => void"
        }
      },
      "onPaste": {
        "defaultValue": null,
        "description": "",
        "name": "onPaste",
        "required": false,
        "type": {
          "name": "(event: ClipboardEvent<HTMLElement>) => void"
        }
      },
      "onPasteCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPasteCapture",
        "required": false,
        "type": {
          "name": "(event: ClipboardEvent<HTMLElement>) => void"
        }
      },
      "onCompositionEnd": {
        "defaultValue": null,
        "description": "",
        "name": "onCompositionEnd",
        "required": false,
        "type": {
          "name": "(event: CompositionEvent<HTMLElement>) => void"
        }
      },
      "onCompositionEndCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onCompositionEndCapture",
        "required": false,
        "type": {
          "name": "(event: CompositionEvent<HTMLElement>) => void"
        }
      },
      "onCompositionStart": {
        "defaultValue": null,
        "description": "",
        "name": "onCompositionStart",
        "required": false,
        "type": {
          "name": "(event: CompositionEvent<HTMLElement>) => void"
        }
      },
      "onCompositionStartCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onCompositionStartCapture",
        "required": false,
        "type": {
          "name": "(event: CompositionEvent<HTMLElement>) => void"
        }
      },
      "onCompositionUpdate": {
        "defaultValue": null,
        "description": "",
        "name": "onCompositionUpdate",
        "required": false,
        "type": {
          "name": "(event: CompositionEvent<HTMLElement>) => void"
        }
      },
      "onCompositionUpdateCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onCompositionUpdateCapture",
        "required": false,
        "type": {
          "name": "(event: CompositionEvent<HTMLElement>) => void"
        }
      },
      "onFocus": {
        "defaultValue": null,
        "description": "",
        "name": "onFocus",
        "required": false,
        "type": {
          "name": "(event: FocusEvent<HTMLElement>) => void"
        }
      },
      "onFocusCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onFocusCapture",
        "required": false,
        "type": {
          "name": "(event: FocusEvent<HTMLElement>) => void"
        }
      },
      "onBlur": {
        "defaultValue": null,
        "description": "",
        "name": "onBlur",
        "required": false,
        "type": {
          "name": "(event: FocusEvent<HTMLElement>) => void"
        }
      },
      "onBlurCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onBlurCapture",
        "required": false,
        "type": {
          "name": "(event: FocusEvent<HTMLElement>) => void"
        }
      },
      "onChange": {
        "defaultValue": null,
        "description": "",
        "name": "onChange",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onChangeCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onChangeCapture",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onBeforeInput": {
        "defaultValue": null,
        "description": "",
        "name": "onBeforeInput",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onBeforeInputCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onBeforeInputCapture",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onInput": {
        "defaultValue": null,
        "description": "",
        "name": "onInput",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onInputCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onInputCapture",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onReset": {
        "defaultValue": null,
        "description": "",
        "name": "onReset",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onResetCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onResetCapture",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onSubmit": {
        "defaultValue": null,
        "description": "",
        "name": "onSubmit",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onSubmitCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onSubmitCapture",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onInvalid": {
        "defaultValue": null,
        "description": "",
        "name": "onInvalid",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onInvalidCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onInvalidCapture",
        "required": false,
        "type": {
          "name": "(event: FormEvent<HTMLElement>) => void"
        }
      },
      "onLoad": {
        "defaultValue": null,
        "description": "",
        "name": "onLoad",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onLoadCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onLoadCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onError": {
        "defaultValue": null,
        "description": "",
        "name": "onError",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onErrorCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onErrorCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onKeyDown": {
        "defaultValue": null,
        "description": "",
        "name": "onKeyDown",
        "required": false,
        "type": {
          "name": "(event: KeyboardEvent<HTMLElement>) => void"
        }
      },
      "onKeyDownCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onKeyDownCapture",
        "required": false,
        "type": {
          "name": "(event: KeyboardEvent<HTMLElement>) => void"
        }
      },
      "onKeyPress": {
        "defaultValue": null,
        "description": "",
        "name": "onKeyPress",
        "required": false,
        "type": {
          "name": "(event: KeyboardEvent<HTMLElement>) => void"
        }
      },
      "onKeyPressCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onKeyPressCapture",
        "required": false,
        "type": {
          "name": "(event: KeyboardEvent<HTMLElement>) => void"
        }
      },
      "onKeyUp": {
        "defaultValue": null,
        "description": "",
        "name": "onKeyUp",
        "required": false,
        "type": {
          "name": "(event: KeyboardEvent<HTMLElement>) => void"
        }
      },
      "onKeyUpCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onKeyUpCapture",
        "required": false,
        "type": {
          "name": "(event: KeyboardEvent<HTMLElement>) => void"
        }
      },
      "onAbort": {
        "defaultValue": null,
        "description": "",
        "name": "onAbort",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onAbortCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onAbortCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onCanPlay": {
        "defaultValue": null,
        "description": "",
        "name": "onCanPlay",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onCanPlayCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onCanPlayCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onCanPlayThrough": {
        "defaultValue": null,
        "description": "",
        "name": "onCanPlayThrough",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onCanPlayThroughCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onCanPlayThroughCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onDurationChange": {
        "defaultValue": null,
        "description": "",
        "name": "onDurationChange",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onDurationChangeCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onDurationChangeCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onEmptied": {
        "defaultValue": null,
        "description": "",
        "name": "onEmptied",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onEmptiedCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onEmptiedCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onEncrypted": {
        "defaultValue": null,
        "description": "",
        "name": "onEncrypted",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onEncryptedCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onEncryptedCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onEnded": {
        "defaultValue": null,
        "description": "",
        "name": "onEnded",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onEndedCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onEndedCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onLoadedData": {
        "defaultValue": null,
        "description": "",
        "name": "onLoadedData",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onLoadedDataCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onLoadedDataCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onLoadedMetadata": {
        "defaultValue": null,
        "description": "",
        "name": "onLoadedMetadata",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onLoadedMetadataCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onLoadedMetadataCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onLoadStart": {
        "defaultValue": null,
        "description": "",
        "name": "onLoadStart",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onLoadStartCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onLoadStartCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onPause": {
        "defaultValue": null,
        "description": "",
        "name": "onPause",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onPauseCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPauseCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onPlay": {
        "defaultValue": null,
        "description": "",
        "name": "onPlay",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onPlayCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPlayCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onPlaying": {
        "defaultValue": null,
        "description": "",
        "name": "onPlaying",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onPlayingCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPlayingCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onProgress": {
        "defaultValue": null,
        "description": "",
        "name": "onProgress",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onProgressCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onProgressCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onRateChange": {
        "defaultValue": null,
        "description": "",
        "name": "onRateChange",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onRateChangeCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onRateChangeCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onSeeked": {
        "defaultValue": null,
        "description": "",
        "name": "onSeeked",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onSeekedCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onSeekedCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onSeeking": {
        "defaultValue": null,
        "description": "",
        "name": "onSeeking",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onSeekingCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onSeekingCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onStalled": {
        "defaultValue": null,
        "description": "",
        "name": "onStalled",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onStalledCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onStalledCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onSuspend": {
        "defaultValue": null,
        "description": "",
        "name": "onSuspend",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onSuspendCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onSuspendCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onTimeUpdate": {
        "defaultValue": null,
        "description": "",
        "name": "onTimeUpdate",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onTimeUpdateCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onTimeUpdateCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onVolumeChange": {
        "defaultValue": null,
        "description": "",
        "name": "onVolumeChange",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onVolumeChangeCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onVolumeChangeCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onWaiting": {
        "defaultValue": null,
        "description": "",
        "name": "onWaiting",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onWaitingCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onWaitingCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onAuxClick": {
        "defaultValue": null,
        "description": "",
        "name": "onAuxClick",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onAuxClickCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onAuxClickCapture",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onClick": {
        "defaultValue": null,
        "description": "",
        "name": "onClick",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onClickCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onClickCapture",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onContextMenu": {
        "defaultValue": null,
        "description": "",
        "name": "onContextMenu",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onContextMenuCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onContextMenuCapture",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onDoubleClick": {
        "defaultValue": null,
        "description": "",
        "name": "onDoubleClick",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onDoubleClickCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onDoubleClickCapture",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onDrag": {
        "defaultValue": null,
        "description": "",
        "name": "onDrag",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onDragCapture",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragEnd": {
        "defaultValue": null,
        "description": "",
        "name": "onDragEnd",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragEndCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onDragEndCapture",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragEnter": {
        "defaultValue": null,
        "description": "",
        "name": "onDragEnter",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragEnterCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onDragEnterCapture",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragExit": {
        "defaultValue": null,
        "description": "",
        "name": "onDragExit",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragExitCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onDragExitCapture",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragLeave": {
        "defaultValue": null,
        "description": "",
        "name": "onDragLeave",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragLeaveCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onDragLeaveCapture",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragOver": {
        "defaultValue": null,
        "description": "",
        "name": "onDragOver",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragOverCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onDragOverCapture",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragStart": {
        "defaultValue": null,
        "description": "",
        "name": "onDragStart",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDragStartCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onDragStartCapture",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDrop": {
        "defaultValue": null,
        "description": "",
        "name": "onDrop",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onDropCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onDropCapture",
        "required": false,
        "type": {
          "name": "(event: DragEvent<HTMLElement>) => void"
        }
      },
      "onMouseDown": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseDown",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onMouseDownCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseDownCapture",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onMouseEnter": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseEnter",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onMouseLeave": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseLeave",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onMouseMove": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseMove",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onMouseMoveCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseMoveCapture",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onMouseOut": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseOut",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onMouseOutCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseOutCapture",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onMouseOver": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseOver",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onMouseOverCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseOverCapture",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onMouseUp": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseUp",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onMouseUpCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onMouseUpCapture",
        "required": false,
        "type": {
          "name": "(event: MouseEvent<HTMLElement, MouseEvent>) => void"
        }
      },
      "onSelect": {
        "defaultValue": null,
        "description": "",
        "name": "onSelect",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onSelectCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onSelectCapture",
        "required": false,
        "type": {
          "name": "(event: SyntheticEvent<HTMLElement, Event>) => void"
        }
      },
      "onTouchCancel": {
        "defaultValue": null,
        "description": "",
        "name": "onTouchCancel",
        "required": false,
        "type": {
          "name": "(event: TouchEvent<HTMLElement>) => void"
        }
      },
      "onTouchCancelCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onTouchCancelCapture",
        "required": false,
        "type": {
          "name": "(event: TouchEvent<HTMLElement>) => void"
        }
      },
      "onTouchEnd": {
        "defaultValue": null,
        "description": "",
        "name": "onTouchEnd",
        "required": false,
        "type": {
          "name": "(event: TouchEvent<HTMLElement>) => void"
        }
      },
      "onTouchEndCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onTouchEndCapture",
        "required": false,
        "type": {
          "name": "(event: TouchEvent<HTMLElement>) => void"
        }
      },
      "onTouchMove": {
        "defaultValue": null,
        "description": "",
        "name": "onTouchMove",
        "required": false,
        "type": {
          "name": "(event: TouchEvent<HTMLElement>) => void"
        }
      },
      "onTouchMoveCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onTouchMoveCapture",
        "required": false,
        "type": {
          "name": "(event: TouchEvent<HTMLElement>) => void"
        }
      },
      "onTouchStart": {
        "defaultValue": null,
        "description": "",
        "name": "onTouchStart",
        "required": false,
        "type": {
          "name": "(event: TouchEvent<HTMLElement>) => void"
        }
      },
      "onTouchStartCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onTouchStartCapture",
        "required": false,
        "type": {
          "name": "(event: TouchEvent<HTMLElement>) => void"
        }
      },
      "onPointerDown": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerDown",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerDownCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerDownCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerMove": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerMove",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerMoveCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerMoveCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerUp": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerUp",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerUpCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerUpCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerCancel": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerCancel",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerCancelCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerCancelCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerEnter": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerEnter",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerEnterCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerEnterCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerLeave": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerLeave",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerLeaveCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerLeaveCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerOver": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerOver",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerOverCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerOverCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerOut": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerOut",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onPointerOutCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onPointerOutCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onGotPointerCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onGotPointerCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onGotPointerCaptureCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onGotPointerCaptureCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onLostPointerCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onLostPointerCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onLostPointerCaptureCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onLostPointerCaptureCapture",
        "required": false,
        "type": {
          "name": "(event: PointerEvent<HTMLElement>) => void"
        }
      },
      "onScroll": {
        "defaultValue": null,
        "description": "",
        "name": "onScroll",
        "required": false,
        "type": {
          "name": "(event: UIEvent<HTMLElement>) => void"
        }
      },
      "onScrollCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onScrollCapture",
        "required": false,
        "type": {
          "name": "(event: UIEvent<HTMLElement>) => void"
        }
      },
      "onWheel": {
        "defaultValue": null,
        "description": "",
        "name": "onWheel",
        "required": false,
        "type": {
          "name": "(event: WheelEvent<HTMLElement>) => void"
        }
      },
      "onWheelCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onWheelCapture",
        "required": false,
        "type": {
          "name": "(event: WheelEvent<HTMLElement>) => void"
        }
      },
      "onAnimationStart": {
        "defaultValue": null,
        "description": "",
        "name": "onAnimationStart",
        "required": false,
        "type": {
          "name": "(event: AnimationEvent<HTMLElement>) => void"
        }
      },
      "onAnimationStartCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onAnimationStartCapture",
        "required": false,
        "type": {
          "name": "(event: AnimationEvent<HTMLElement>) => void"
        }
      },
      "onAnimationEnd": {
        "defaultValue": null,
        "description": "",
        "name": "onAnimationEnd",
        "required": false,
        "type": {
          "name": "(event: AnimationEvent<HTMLElement>) => void"
        }
      },
      "onAnimationEndCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onAnimationEndCapture",
        "required": false,
        "type": {
          "name": "(event: AnimationEvent<HTMLElement>) => void"
        }
      },
      "onAnimationIteration": {
        "defaultValue": null,
        "description": "",
        "name": "onAnimationIteration",
        "required": false,
        "type": {
          "name": "(event: AnimationEvent<HTMLElement>) => void"
        }
      },
      "onAnimationIterationCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onAnimationIterationCapture",
        "required": false,
        "type": {
          "name": "(event: AnimationEvent<HTMLElement>) => void"
        }
      },
      "onTransitionEnd": {
        "defaultValue": null,
        "description": "",
        "name": "onTransitionEnd",
        "required": false,
        "type": {
          "name": "(event: TransitionEvent<HTMLElement>) => void"
        }
      },
      "onTransitionEndCapture": {
        "defaultValue": null,
        "description": "",
        "name": "onTransitionEndCapture",
        "required": false,
        "type": {
          "name": "(event: TransitionEvent<HTMLElement>) => void"
        }
      },
      "key": {
        "defaultValue": null,
        "description": "",
        "name": "key",
        "required": false,
        "type": {
          "name": "ReactText"
        }
      },
      "ref": {
        "defaultValue": null,
        "description": "",
        "name": "ref",
        "required": false,
        "type": {
          "name": "Ref<HTMLElement>"
        }
      }
    }
  }; // @ts-ignore

  if (typeof STORYBOOK_REACT_CLASSES !== "undefined") // @ts-ignore
    STORYBOOK_REACT_CLASSES["components/Button/index.tsx#Button"] = {
      docgenInfo: Button.__docgenInfo,
      name: "Button",
      path: "components/Button/index.tsx#Button"
    };
} catch (__react_docgen_typescript_loader_error) {}
// CONCATENATED MODULE: ./components/index.ts
/* concated harmony reexport CustomButton */__webpack_require__.d(__webpack_exports__, "CustomButton", function() { return CustomButton; });
/* concated harmony reexport ButtonLink */__webpack_require__.d(__webpack_exports__, "ButtonLink", function() { return ButtonLink; });
/* concated harmony reexport Button */__webpack_require__.d(__webpack_exports__, "Button", function() { return Button; });


/***/ })
/******/ ]);