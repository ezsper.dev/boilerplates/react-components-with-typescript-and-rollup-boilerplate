import { basename } from 'path';
import webpack from 'webpack';
import libraryConfig, { LibraryOutput } from '../webpack/library.config';
import { runCompiler } from '../utils/webpack';
import pkg from '../../package.json';

const library = 'MyLibrary';

(async () => {
  try {
    const outputs: LibraryOutput[] = [
      {
        library,
        libraryTarget: 'commonjs',
        filename: basename(pkg.main),
      },
      {
        library,
        libraryTarget: 'commonjs2',
        filename: basename(pkg.module),
      },
      {
        library,
        libraryTarget: 'var',
        filename: basename(pkg.browser),
      },
    ];
    const compiler = webpack(outputs.map(libraryConfig));
    await runCompiler(compiler);
    console.info('Completed');
  } catch (error) {
    console.error(error.stack);
    process.exit(2);
  }
})();
