import { resolve as resolvePath } from 'path';
import webpack from 'webpack';
import webpackNodeExternals from 'webpack-node-externals';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import webpackMerge from 'webpack-merge';
import commonConfig from './common.config';

export interface LibraryOutput extends webpack.Output {}

const projectPath = resolvePath(__dirname, '../..');

export default (output: LibraryOutput) =>
  webpackMerge(commonConfig({ target: 'library' }), <webpack.Configuration>{
    entry: './components/index',
    context: projectPath,
    plugins: [
      new MiniCssExtractPlugin({
        // Options similar to the same options in webpackOptions.output
        // all options are optional
        filename: 'styles.css',
        chunkFilename: 'styles.css',
      }),
    ],
    optimization: {
      minimize: false,
      splitChunks: {
        cacheGroups: {
          styles: {
            name: 'styles',
            test: /\.css$/,
            chunks: 'all',
          },
        },
      },
    },
    output: {
      path: resolvePath(projectPath, 'dist'),
      ...output,
    },
    externals: [webpackNodeExternals()],
  });
