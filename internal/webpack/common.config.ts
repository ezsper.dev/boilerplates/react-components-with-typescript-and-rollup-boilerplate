import { resolve as resolvePath } from 'path';
import webpack from 'webpack';
import * as HappyPack from 'happypack';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';

export interface CommonOptions {
  target: 'storybook' | 'library';
}

export default ({ target }: CommonOptions) =>
  <webpack.Configuration>{
    plugins: [
      ...Object.entries(<{ [id: string]: HappyPack.PluginOptions }>{
        javascript: {
          loaders: [
            {
              loader: require.resolve('babel-loader'),
            },
          ],
        },
        typescript: {
          loaders: [
            {
              loader: require.resolve('react-docgen-typescript-loader'),
              options: {
                tsconfigPath: resolvePath(__dirname, '../../tsconfig.json'),
              },
            },
          ],
        },
        style: {
          loaders: [
            ...(target === 'library'
              ? [
                  {
                    loader: MiniCssExtractPlugin.loader,
                    options: {
                      hmr: process.env.NODE_ENV === 'development',
                    },
                  },
                ]
              : [
                  {
                    loader: require.resolve('style-loader'),
                    options: {},
                  },
                ]),
            {
              loader: require.resolve('css-loader'),
              options: {
                importLoaders: 1,
                modules: true,
              },
            },
            {
              loader: require.resolve('postcss-loader'),
              options: {
                ident: 'postcss',
                postcss: {},
              },
            },
          ],
        },
        file: {
          id: 'file',
          loaders: [
            {
              loader: require.resolve('file-loader'),
              options: {
                name: 'static/media/[target].[hash:8].[ext]',
              },
            },
          ],
        },
        url: {
          id: 'url',
          loaders: [
            {
              loader: require.resolve('url-loader'),
              options: {
                limit: 10000,
                name: 'static/media/[target].[hash:8].[ext]',
              },
            },
          ],
        },
      }).map(([id, options]) => new HappyPack({ id, ...options })),
    ],
    module: {
      rules: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          use: ['happypack/loader?id=javascript'],
        },
        {
          test: /\.tsx?$/,
          use: ['happypack/loader?id=javascript', 'happypack/loader?id=typescript'],
        },
        {
          test: /\.css$/,
          sideEffects: true,
          use: ['happypack/loader?id=style'],
        },
        {
          test: /\.(svg|ico|jpg|jpeg|png|gif|eot|otf|webp|ttf|woff|woff2|cur|ani)(\?.*)?$/,
          use: ['happypack/loader?id=file'],
        },
        {
          test: /\.(mp4|webm|wav|mp3|m4a|aac|oga)(\?.*)?$/,
          use: ['happypack/loader?id=url'],
        },
      ],
    },
    resolve: {
      extensions: ['.mjs', '.ts', '.tsx', '.js', '.jsx', '.json', '.mdx'],
    },
  };
