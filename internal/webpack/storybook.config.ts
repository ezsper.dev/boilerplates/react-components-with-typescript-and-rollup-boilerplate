import webpack from 'webpack';
import * as webpackMerge from 'webpack-merge';
import commonConfig from './common.config';
// @ts-ignore
import * as createCompiler from '@storybook/addon-docs/mdx-compiler-plugin';
export default webpackMerge(commonConfig({ target: 'storybook' }), <webpack.Configuration>{
  module: {
    rules: [
      {
        test: /\.mdx$/,
        use: [
          'happypack/loader?id=javascript',
          {
            loader: '@mdx-js/loader',
            options: {
              compilers: [createCompiler({})],
            },
          },
        ],
      },
    ],
  },
});
