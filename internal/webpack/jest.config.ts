import webpack from 'webpack';
import webpackMerge from 'webpack-merge';
import commonConfig from './common.config';
export default webpackMerge(commonConfig({ target: 'library' }), <webpack.Configuration>{});
