const fs = require('fs-extra');
const resolvePath = require('path').resolve;

const projectPath = resolvePath(__dirname, '../..');
const babelConfig = fs.readJSONSync(resolvePath(projectPath, '.babelrc'));

require('@babel/register')(
  Object.assign({}, babelConfig, {
    babelrc: false,
    extensions: ['.js', '.jsx', '.ts', '.tsx'],
  }),
);
