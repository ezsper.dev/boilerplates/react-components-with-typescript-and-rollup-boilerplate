var resolvePath = require('path').resolve;
var fs = require('fs-extra');
var projectPath = resolvePath(__dirname, '../..');
var tsconfig = resolvePath(projectPath, 'tsconfig.json');
var compilerOptions = fs.readJsonSync(tsconfig).compilerOptions;
var compilerOptions.baseUrl = resolvePath(projectPath, compilerOptions.baseUrl || '.');

require('ts-node').register({
  transpileOnly: true,
  project: tsconfig,
  compilerOptions: compilerOptions,
});

var paths = { '*': [] };

Object.assign(paths, compilerOptions.paths);

const index = paths['*'].indexOf('./node_modules/*');

if (index >= 0) {
  paths['*'][index] = './node_modules/**/*';
} else {
  paths['*'] = ['./node_modules/**/*'].concat(paths['*']);
}

require('tsconfig-paths').register({
  baseUrl: compilerOptions.baseUrl,
  paths: paths,
});