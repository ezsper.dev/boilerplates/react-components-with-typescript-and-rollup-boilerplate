const fs = require('fs-extra');
const resolvePath = require('path').resolve;

const projectPath = resolvePath(__dirname, '../..');

/* important for webpack using happypack */
{
  let hasHappypack = false;
  try {
    require.resolve('happypack');
    hasHappypack = true;
  } catch (error) {}

  if (hasHappypack) {
    const childProcess = require('child_process');
    const originalFork = childProcess.fork;
    childProcess.fork = function() {
      const args = Array.prototype.slice.call(arguments);
      if (args[0] === require.resolve('happypack/lib/HappyWorkerChannel.js')) {
        args[2] = Object.assign({}, args[2], {
          execArgv: ['-r', resolvePath(__dirname, 'index.js')].concat(
            args[2] != null && args[2].execArgv != null ? args[2].execArgv : [],
          ),
        });
      }
      return originalFork.apply(this, args);
    };
  }
}

let hasTypescript = false;
if (fs.existsSync(resolvePath(projectPath, 'tsconfig.json'))) {
  try {
    require('./typescript');
    hasTypescript = true;
  } catch (error) {}
}

require('./babel');
