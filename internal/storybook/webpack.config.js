require('../register');
const webpackMerge = require('webpack-merge');
const storybookConfig = require('../webpack/storybook.config').default;

module.exports = ({ config, mode }) => {
  const webpackConfig = webpackMerge(config, storybookConfig);
  return {
    ...webpackConfig,
    module: {
      ...webpackConfig.module,
      ...(storybookConfig.module &&
        storybookConfig.module &&
        storybookConfig.module.rules && { rules: storybookConfig.module.rules }),
    },
  };
};
