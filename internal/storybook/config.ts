// @ts-ignore
import { load, addParameters } from '@storybook/react';

addParameters({
  options: {
    docs: {
      inlineStories: true,
    },
  },
});

// automatically import all files ending in *.stories.tsx
load(require.context('../../components', true, /(^|.)stories\.([jt]x?|mdx)$/));
