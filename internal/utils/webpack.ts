import webpack from 'webpack';

export const runCompiler = (compiler: webpack.Compiler | webpack.MultiCompiler) => {
  return new Promise<webpack.Stats>((resolve, reject) => {
    return compiler.run((error, stats) => {
      if (error) {
        reject(error);
      } else {
        resolve(stats);
      }
    });
  });
};
