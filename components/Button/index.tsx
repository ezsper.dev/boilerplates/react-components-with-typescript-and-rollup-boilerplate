import React, {
  DetailedHTMLFactory,
  Ref,
  ReactNode,
  forwardRef,
  HTMLProps,
  ReactHTML,
} from 'react';
import classnames from 'classnames';
import styles from './styles.css';

export interface CustomButtonProp<T> extends HTMLProps<T> {
  ref?: Ref<T>;
  children?: ReactNode;
}

export const CustomButton = <
  C extends keyof ReactHTML,
  T extends ReactHTML[C] extends DetailedHTMLFactory<any, infer U> ? U : HTMLElement
>(
  component: C,
) =>
  forwardRef<T, CustomButtonProp<T>>(({ className, ...rest }, ref) => {
    return React.createElement(component, {
      className: classnames(styles.button, className),
      ...rest,
      ref,
    });
  });

/**
 * Custom buttons
 */
export const Button = Object.assign(CustomButton('button'), {
  Link: CustomButton('a'),
});

export const ButtonLink = Button.Link;

export default Button;
