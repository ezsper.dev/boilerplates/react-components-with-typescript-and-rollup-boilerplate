import React from 'react';
import { mount } from 'enzyme';
import sinon from 'sinon';

import Button from '.';

describe('button', () => {
  it('should render a button element', () => {
    const wrapper = mount(<Button>example</Button>);

    expect(wrapper.children('button')).toHaveLength(1);
  });

  it('should render an anchor element', () => {
    const wrapper = mount(<Button.Link href="/">example</Button.Link>);

    expect(wrapper.children('a')).toHaveLength(1);
  });

  it('allows attributes to be set', () => {
    const wrapper = mount(
      <Button id="test" className="test">
        example
      </Button>,
    );

    expect(wrapper.hasClass('test')).toBe(true);
    expect(wrapper.is('#test')).toBe(true);
  });

  it('matches snapshot', () => {
    const wrapper = mount(<Button className="test">example</Button>);

    expect(wrapper).toMatchSnapshot();
  });

  it('to have click', () => {
    const clickCallback = sinon.spy();
    const wrapper = mount(<Button onClick={clickCallback}>example</Button>);
    wrapper.simulate('click');

    expect(clickCallback).toHaveProperty('callCount', 1);
  });
});
