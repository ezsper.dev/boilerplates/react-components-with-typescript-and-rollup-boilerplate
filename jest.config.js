// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
  clearMocks: true,
  collectCoverage: true,
  collectCoverageFrom: [
    '<rootDir>/components/**/*.{j,t}s?(x)',
    '!<rootDir>/components/**/{stories,example,fixtures}.{j,t}s?(x)',
    '<rootDir>/packages/*/src/**/*.{j,t}s?(x)',
    '!<rootDir>/packages/*/src/**/{stories,example,fixtures}.{j,t}s?(x)',
  ],
  coverageDirectory: './coverage/',
  coverageThreshold: {
    global: {
      branches: 95,
      functions: 95,
      lines: 95,
      statements: 95,
    },
  },
  moduleFileExtensions: ['web.js', 'js', 'json', 'web.jsx', 'jsx', 'ts', 'tsx', 'node'],
  setupFilesAfterEnv: ['<rootDir>/internal/jest/setupTests.js'],
  snapshotSerializers: ['enzyme-to-json/serializer'],
  testEnvironment: 'jsdom',
  testMatch: [
    '**/packages/*/src/**/?(*.)(spec|test).(js|ts)?(x)',
    '**/components/**/?(*.)(spec|test).(js|ts)?(x)',
  ],
  moduleNameMapper: {
    '^.+\\.(css|styl|less|sass|scss)$': 'identity-obj-proxy',
  },
  transform: {
    '^.+\\.[jt]sx?$': 'babel-jest',
    '^.+\\.(png|jpg|ttf|woff|woff2)$': 'jest-transform-stub',
  },
  transformIgnorePatterns: ['[/\\\\]node_modules[/\\\\].+\\.[jt]sx?$'],
};
