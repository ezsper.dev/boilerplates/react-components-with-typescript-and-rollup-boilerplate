const RollupTypeScriptBabel = (function(exports) {
  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value,
        enumerable: true,
        configurable: true,
        writable: true,
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function ownKeys(object, enumerableOnly) {
    const keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      let symbols = Object.getOwnPropertySymbols(object);
      if (enumerableOnly)
        symbols = symbols.filter(function(sym) {
          return Object.getOwnPropertyDescriptor(object, sym).enumerable;
        });
      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread2(target) {
    for (let i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(source, true).forEach(function(key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(source).forEach(function(key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _objectWithoutPropertiesLoose(source, excluded) {
    if (source == null) return {};
    const target = {};
    const sourceKeys = Object.keys(source);
    let key;
    let i;

    for (i = 0; i < sourceKeys.length; i++) {
      key = sourceKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      target[key] = source[key];
    }

    return target;
  }

  function _objectWithoutProperties(source, excluded) {
    if (source == null) return {};

    const target = _objectWithoutPropertiesLoose(source, excluded);

    let key;
    let i;

    if (Object.getOwnPropertySymbols) {
      const sourceSymbolKeys = Object.getOwnPropertySymbols(source);

      for (i = 0; i < sourceSymbolKeys.length; i++) {
        key = sourceSymbolKeys[i];
        if (excluded.indexOf(key) >= 0) continue;
        if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
        target[key] = source[key];
      }
    }

    return target;
  }

  function createCommonjsModule(fn, module) {
    return (module = { exports: {} }), fn(module, module.exports), module.exports;
  }

  /*
  object-assign
  (c) Sindre Sorhus
  @license MIT
  */
  /* eslint-disable no-unused-vars */
  const { getOwnPropertySymbols } = Object;
  const { hasOwnProperty } = Object.prototype;
  const propIsEnumerable = Object.prototype.propertyIsEnumerable;

  function toObject(val) {
    if (val === null || val === undefined) {
      throw new TypeError('Object.assign cannot be called with null or undefined');
    }

    return Object(val);
  }

  function shouldUseNative() {
    try {
      if (!Object.assign) {
        return false;
      }

      // Detect buggy property enumeration order in older V8 versions.

      // https://bugs.chromium.org/p/v8/issues/detail?id=4118
      const test1 = new String('abc'); // eslint-disable-line no-new-wrappers
      test1[5] = 'de';
      if (Object.getOwnPropertyNames(test1)[0] === '5') {
        return false;
      }

      // https://bugs.chromium.org/p/v8/issues/detail?id=3056
      const test2 = {};
      for (let i = 0; i < 10; i++) {
        test2[`_${String.fromCharCode(i)}`] = i;
      }
      const order2 = Object.getOwnPropertyNames(test2).map(function(n) {
        return test2[n];
      });
      if (order2.join('') !== '0123456789') {
        return false;
      }

      // https://bugs.chromium.org/p/v8/issues/detail?id=3056
      const test3 = {};
      'abcdefghijklmnopqrst'.split('').forEach(function(letter) {
        test3[letter] = letter;
      });
      if (Object.keys(Object.assign({}, test3)).join('') !== 'abcdefghijklmnopqrst') {
        return false;
      }

      return true;
    } catch (err) {
      // We don't expect any of the above to throw, but better to be safe.
      return false;
    }
  }

  const objectAssign = shouldUseNative()
    ? Object.assign
    : function(target, source) {
        let from;
        const to = toObject(target);
        let symbols;

        for (let s = 1; s < arguments.length; s++) {
          from = Object(arguments[s]);

          for (const key in from) {
            if (hasOwnProperty.call(from, key)) {
              to[key] = from[key];
            }
          }

          if (getOwnPropertySymbols) {
            symbols = getOwnPropertySymbols(from);
            for (let i = 0; i < symbols.length; i++) {
              if (propIsEnumerable.call(from, symbols[i])) {
                to[symbols[i]] = from[symbols[i]];
              }
            }
          }
        }

        return to;
      };

  const n = typeof Symbol === 'function' && Symbol.for;
  const p = n ? Symbol.for('react.element') : 60103;
  const q = n ? Symbol.for('react.portal') : 60106;
  const r = n ? Symbol.for('react.fragment') : 60107;
  const t = n ? Symbol.for('react.strict_mode') : 60108;
  const u = n ? Symbol.for('react.profiler') : 60114;
  const v = n ? Symbol.for('react.provider') : 60109;
  const w = n ? Symbol.for('react.context') : 60110;
  const x = n ? Symbol.for('react.concurrent_mode') : 60111;
  const y = n ? Symbol.for('react.forward_ref') : 60112;
  const z = n ? Symbol.for('react.suspense') : 60113;
  const aa = n ? Symbol.for('react.memo') : 60115;
  const ba = n ? Symbol.for('react.lazy') : 60116;
  const A = typeof Symbol === 'function' && Symbol.iterator;
  function ca(a, b, d, c, e, g, h, f) {
    if (!a) {
      a = void 0;
      if (void 0 === b)
        a = Error(
          'Minified exception occurred; use the non-minified dev environment for the full error message and additional helpful warnings.',
        );
      else {
        const l = [d, c, e, g, h, f];
        let m = 0;
        a = Error(
          b.replace(/%s/g, function() {
            return l[m++];
          }),
        );
        a.name = 'Invariant Violation';
      }
      a.framesToPop = 1;
      throw a;
    }
  }
  function B(a) {
    for (
      var b = arguments.length - 1,
        d = `https://reactjs.org/docs/error-decoder.html?invariant=${a}`,
        c = 0;
      c < b;
      c++
    )
      d += `&args[]=${encodeURIComponent(arguments[c + 1])}`;
    ca(
      !1,
      `Minified React error #${a}; visit %s for the full message or use the non-minified dev environment for full errors and additional helpful warnings. `,
      d,
    );
  }
  const C = {
    isMounted() {
      return !1;
    },
    enqueueForceUpdate() {},
    enqueueReplaceState() {},
    enqueueSetState() {},
  };
  const D = {};
  function E(a, b, d) {
    this.props = a;
    this.context = b;
    this.refs = D;
    this.updater = d || C;
  }
  E.prototype.isReactComponent = {};
  E.prototype.setState = function(a, b) {
    typeof a !== 'object' && typeof a !== 'function' && a != null ? B('85') : void 0;
    this.updater.enqueueSetState(this, a, b, 'setState');
  };
  E.prototype.forceUpdate = function(a) {
    this.updater.enqueueForceUpdate(this, a, 'forceUpdate');
  };
  function F() {}
  F.prototype = E.prototype;
  function G(a, b, d) {
    this.props = a;
    this.context = b;
    this.refs = D;
    this.updater = d || C;
  }
  const H = (G.prototype = new F());
  H.constructor = G;
  objectAssign(H, E.prototype);
  H.isPureReactComponent = !0;
  const I = { current: null };
  const J = { current: null };
  const K = Object.prototype.hasOwnProperty;
  const L = { key: !0, ref: !0, __self: !0, __source: !0 };
  function M(a, b, d) {
    let c = void 0;
    const e = {};
    let g = null;
    let h = null;
    if (b != null)
      for (c in (void 0 !== b.ref && (h = b.ref), void 0 !== b.key && (g = `${b.key}`), b))
        K.call(b, c) && !L.hasOwnProperty(c) && (e[c] = b[c]);
    let f = arguments.length - 2;
    if (f === 1) e.children = d;
    else if (f > 1) {
      for (var l = Array(f), m = 0; m < f; m++) l[m] = arguments[m + 2];
      e.children = l;
    }
    if (a && a.defaultProps) for (c in ((f = a.defaultProps), f)) void 0 === e[c] && (e[c] = f[c]);
    return { $$typeof: p, type: a, key: g, ref: h, props: e, _owner: J.current };
  }
  function da(a, b) {
    return { $$typeof: p, type: a.type, key: b, ref: a.ref, props: a.props, _owner: a._owner };
  }
  function N(a) {
    return typeof a === 'object' && a !== null && a.$$typeof === p;
  }
  function escape(a) {
    const b = { '=': '=0', ':': '=2' };
    return `$${`${a}`.replace(/[=:]/g, function(a) {
      return b[a];
    })}`;
  }
  const O = /\/+/g;
  const P = [];
  function Q(a, b, d, c) {
    if (P.length) {
      const e = P.pop();
      e.result = a;
      e.keyPrefix = b;
      e.func = d;
      e.context = c;
      e.count = 0;
      return e;
    }
    return { result: a, keyPrefix: b, func: d, context: c, count: 0 };
  }
  function R(a) {
    a.result = null;
    a.keyPrefix = null;
    a.func = null;
    a.context = null;
    a.count = 0;
    P.length < 10 && P.push(a);
  }
  function S(a, b, d, c) {
    let e = typeof a;
    if (e === 'undefined' || e === 'boolean') a = null;
    let g = !1;
    if (a === null) g = !0;
    else
      switch (e) {
        case 'string':
        case 'number':
          g = !0;
          break;
        case 'object':
          switch (a.$$typeof) {
            case p:
            case q:
              g = !0;
          }
      }
    if (g) return d(c, a, b === '' ? `.${T(a, 0)}` : b), 1;
    g = 0;
    b = b === '' ? '.' : `${b}:`;
    if (Array.isArray(a))
      for (var h = 0; h < a.length; h++) {
        e = a[h];
        var f = b + T(e, h);
        g += S(e, f, d, c);
      }
    else if (
      (a === null || typeof a !== 'object'
        ? (f = null)
        : ((f = (A && a[A]) || a['@@iterator']), (f = typeof f === 'function' ? f : null)),
      typeof f === 'function')
    )
      for (a = f.call(a), h = 0; !(e = a.next()).done; )
        (e = e.value), (f = b + T(e, h++)), (g += S(e, f, d, c));
    else
      e === 'object' &&
        ((d = `${a}`),
        B(
          '31',
          d === '[object Object]' ? `object with keys {${Object.keys(a).join(', ')}}` : d,
          '',
        ));
    return g;
  }
  function U(a, b, d) {
    return a == null ? 0 : S(a, '', b, d);
  }
  function T(a, b) {
    return typeof a === 'object' && a !== null && a.key != null ? escape(a.key) : b.toString(36);
  }
  function ea(a, b) {
    a.func.call(a.context, b, a.count++);
  }
  function fa(a, b, d) {
    const c = a.result;
    const e = a.keyPrefix;
    a = a.func.call(a.context, b, a.count++);
    Array.isArray(a)
      ? V(a, c, d, function(a) {
          return a;
        })
      : a != null &&
        (N(a) &&
          (a = da(
            a,
            e + (!a.key || (b && b.key === a.key) ? '' : `${`${a.key}`.replace(O, '$&/')}/`) + d,
          )),
        c.push(a));
  }
  function V(a, b, d, c, e) {
    let g = '';
    d != null && (g = `${`${d}`.replace(O, '$&/')}/`);
    b = Q(b, g, c, e);
    U(a, fa, b);
    R(b);
  }
  function W() {
    const a = I.current;
    a === null ? B('321') : void 0;
    return a;
  }
  const X = {
    Children: {
      map(a, b, d) {
        if (a == null) return a;
        const c = [];
        V(a, c, null, b, d);
        return c;
      },
      forEach(a, b, d) {
        if (a == null) return a;
        b = Q(null, null, b, d);
        U(a, ea, b);
        R(b);
      },
      count(a) {
        return U(
          a,
          function() {
            return null;
          },
          null,
        );
      },
      toArray(a) {
        const b = [];
        V(a, b, null, function(a) {
          return a;
        });
        return b;
      },
      only(a) {
        N(a) ? void 0 : B('143');
        return a;
      },
    },
    createRef() {
      return { current: null };
    },
    Component: E,
    PureComponent: G,
    createContext(a, b) {
      void 0 === b && (b = null);
      a = {
        $$typeof: w,
        _calculateChangedBits: b,
        _currentValue: a,
        _currentValue2: a,
        _threadCount: 0,
        Provider: null,
        Consumer: null,
      };
      a.Provider = { $$typeof: v, _context: a };
      return (a.Consumer = a);
    },
    forwardRef(a) {
      return { $$typeof: y, render: a };
    },
    lazy(a) {
      return { $$typeof: ba, _ctor: a, _status: -1, _result: null };
    },
    memo(a, b) {
      return { $$typeof: aa, type: a, compare: void 0 === b ? null : b };
    },
    useCallback(a, b) {
      return W().useCallback(a, b);
    },
    useContext(a, b) {
      return W().useContext(a, b);
    },
    useEffect(a, b) {
      return W().useEffect(a, b);
    },
    useImperativeHandle(a, b, d) {
      return W().useImperativeHandle(a, b, d);
    },
    useDebugValue() {},
    useLayoutEffect(a, b) {
      return W().useLayoutEffect(a, b);
    },
    useMemo(a, b) {
      return W().useMemo(a, b);
    },
    useReducer(a, b, d) {
      return W().useReducer(a, b, d);
    },
    useRef(a) {
      return W().useRef(a);
    },
    useState(a) {
      return W().useState(a);
    },
    Fragment: r,
    StrictMode: t,
    Suspense: z,
    createElement: M,
    cloneElement(a, b, d) {
      a === null || void 0 === a ? B('267', a) : void 0;
      let c = void 0;
      const e = objectAssign({}, a.props);
      let g = a.key;
      let h = a.ref;
      let f = a._owner;
      if (b != null) {
        void 0 !== b.ref && ((h = b.ref), (f = J.current));
        void 0 !== b.key && (g = `${b.key}`);
        var l = void 0;
        a.type && a.type.defaultProps && (l = a.type.defaultProps);
        for (c in b)
          K.call(b, c) &&
            !L.hasOwnProperty(c) &&
            (e[c] = void 0 === b[c] && void 0 !== l ? l[c] : b[c]);
      }
      c = arguments.length - 2;
      if (c === 1) e.children = d;
      else if (c > 1) {
        l = Array(c);
        for (let m = 0; m < c; m++) l[m] = arguments[m + 2];
        e.children = l;
      }
      return { $$typeof: p, type: a.type, key: g, ref: h, props: e, _owner: f };
    },
    createFactory(a) {
      const b = M.bind(null, a);
      b.type = a;
      return b;
    },
    isValidElement: N,
    version: '16.8.6',
    unstable_ConcurrentMode: x,
    unstable_Profiler: u,
    __SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED: {
      ReactCurrentDispatcher: I,
      ReactCurrentOwner: J,
      assign: objectAssign,
    },
  };
  const Y = { default: X };
  const Z = (Y && X) || Y;
  const react_production_min = Z.default || Z;

  const react = createCommonjsModule(function(module) {
    {
      module.exports = react_production_min;
    }
  });
  const react_1 = react.Component;
  const react_2 = react.PropTypes;
  const react_3 = react.createElement;
  const react_4 = react.forwardRef;

  const classnames = createCommonjsModule(function(module) {
    /*!
    Copyright (c) 2017 Jed Watson.
    Licensed under the MIT License (MIT), see
    http://jedwatson.github.io/classnames
  */
    /* global define */

    (function() {
      const hasOwn = {}.hasOwnProperty;

      function classNames() {
        const classes = [];

        for (let i = 0; i < arguments.length; i++) {
          const arg = arguments[i];
          if (!arg) continue;

          const argType = typeof arg;

          if (argType === 'string' || argType === 'number') {
            classes.push(arg);
          } else if (Array.isArray(arg) && arg.length) {
            const inner = classNames.apply(null, arg);
            if (inner) {
              classes.push(inner);
            }
          } else if (argType === 'object') {
            for (const key in arg) {
              if (hasOwn.call(arg, key) && arg[key]) {
                classes.push(key);
              }
            }
          }
        }

        return classes.join(' ');
      }

      if (module.exports) {
        classNames.default = classNames;
        module.exports = classNames;
      } else {
        window.classNames = classNames;
      }
    })();
  });

  const styles = /* #__PURE__ */ Object.freeze({
    default: undefined,
  });

  const Button = react_4(function(_ref, ref) {
    const _ref$component = _ref.component;
    const component = _ref$component === void 0 ? 'button' : _ref$component;
    const { className } = _ref;
    const rest = _objectWithoutProperties(_ref, ['component', 'className']);

    return react.createElement(
      component,
      _objectSpread2(
        {
          className: classnames(undefined, className),
        },
        rest,
        {
          ref,
        },
      ),
    );
  });

  exports.Button = Button;

  return exports;
})({});
